﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/CubesConfig", fileName = "CubesConfig", order = 0)]
public class CubesConfigSO : ScriptableObject
{

    public List<GameObject> AwesomeBlocks = new List<GameObject>();
    public List<GameObject> MegaEffects;
    public float BaseSpeed = 1;
    public float AllowedSphereRange;

    public GameObject GetAnAesomeBlock()
    {
        int count = AwesomeBlocks.Count;
        if (count == 0) return null;

        return AwesomeBlocks[Random.Range(0, count)];
    }

    public GameObject GetAnMegaEffect()
    {
        int count = MegaEffects.Count;
        if (count == 0) return null;

        return MegaEffects[Random.Range(0, count)];
    }
}
