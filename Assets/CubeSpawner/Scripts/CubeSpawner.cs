﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour
{
    [SerializeField] private CubesConfigSO _cubeConfig;
    [SerializeField] private GameObject _cubePrefab;
    [SerializeField] private int _maxNumberOfCubes;
    [SerializeField] private float _spawnInterval;

    [Header("Debug Only")] [SerializeField]
    private int _numberOfCubes = 0;

    [SerializeField] private float _lastSpawnElapsedTime = 0;

    private Coroutine _spawnCoroutine;

    private void Start()
    {
        StartSpawn();
    }



    #region "PUBLIC METHODS"

    public void StartSpawn()
    {
        _spawnCoroutine = StartCoroutine(SpawnCoroutine());
    }

    public void StopSpawn()
    {
        if (_spawnCoroutine != null)
        {
            StopCoroutine(_spawnCoroutine);
            _spawnCoroutine = null;
        }
    }

    #endregion

    #region "PRIVATE METHODS"
    private void SpawnNewCube()
    {
        CubeBehaviour newCube = Instantiate(_cubePrefab, Random.onUnitSphere*_cubeConfig.AllowedSphereRange,Quaternion.identity,transform).GetComponent<CubeBehaviour>();
        newCube.SetConfig(_cubeConfig);
        newCube.SetBlock();
        newCube.WhenDestroyed.AddListener(SubCount);
        AddCount();
    }

    private void AddCount()
    {
        _numberOfCubes++;
    }

    private void SubCount()
    {
        _numberOfCubes--;
    }

    private IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            SpawnNewCube();
            _lastSpawnElapsedTime = 0;
            while (_lastSpawnElapsedTime < _spawnInterval)
            {
                _lastSpawnElapsedTime += Time.deltaTime;
                yield return null;
            }

            while (_numberOfCubes >= _maxNumberOfCubes)
                yield return null;
        }
    }

    #endregion
}