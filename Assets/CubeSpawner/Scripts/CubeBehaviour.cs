﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class CubeBehaviour : MonoBehaviour
{

	[SerializeField] private CubesConfigSO _config;
	private Vector3 _velocity;
	private Vector3 _target;
	private Vector3 _randomRotate;
	public UnityEvent WhenDestroyed = new UnityEvent();


	private void Start()
	{
		//if(_config == null) _config = Resources.Load<CubesConfigSO>("CubesConfig");
		_velocity = Random.onUnitSphere;
		_randomRotate = new Vector3(Random.Range(-45,45),Random.Range(-45,45),Random.Range(-45,45));
		SFX.PlayIn3D(SFXKey.SFX_SpawnCube, transform.position, pitchVariation: 0.15f);

	}

	private void Update()
	{
		MoveAround();
		RotateAround();
	}

	private void RotateAround()
	{
		transform.Rotate(_randomRotate*Time.deltaTime*_config.BaseSpeed);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			WhenDestroyed.Invoke();
			SFX.PlayIn3D(SFXKey.SFX_DeathCube, transform.position, pitchVariation: 0.15f);
			Destroy(this.gameObject);
		}
	}

	public void SpawnEffect()
	{
		GameObject toInstantiate = _config.GetAnMegaEffect();
		if (toInstantiate == null) return;
		Instantiate(toInstantiate, transform.position, Quaternion.identity);
	}
	
	public void SetBlock()
	{
		GameObject toInstantiate = _config.GetAnAesomeBlock();
		if (toInstantiate == null) return;
		Destroy(GetComponent<MeshRenderer>());
		Instantiate(toInstantiate, transform);
	}
	
	private void MoveAround()
	{
		Vector3 desiredVel = _target - transform.position;
		_velocity = (_velocity + (desiredVel).normalized).normalized;
		
		transform.position+=(_velocity * Time.deltaTime * _config.BaseSpeed);

		if (desiredVel.magnitude <= _config.BaseSpeed)
		{
			Vector3 newTarget = Random.onUnitSphere * _config.AllowedSphereRange;
			if (newTarget.z < 0)
			{
				newTarget.z = -newTarget.z;
			}
			_target = newTarget+transform.parent.position;
		}
	}

	public void SetConfig(CubesConfigSO config)
	{
		_config = config;
	}
}
