﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MTR.Core
{
    public class MTRReader : MonoBehaviour
    {

        [Header("Debug")]
        [SerializeField] private bool _showDebugMessage;
        [SerializeField] private bool _isReading;
        [SerializeField] private bool _currentPoseIsCorrect;
        [SerializeField] private bool _firstPoseOk;
        [SerializeField] private bool _secondPoseOk;
        [Range(0.0f, 1.0f)]
        [SerializeField] private float _movementStrengh = 0f;

        [Header("Reader Settings")]
        [Range(0f, 10f)]
        [SerializeField] public float DistanceAcceptable = 0.5f;
        [SerializeField] private float _readerSpeed = 0.1f;
        [SerializeField] private bool _compareRotation;
        [SerializeField] private bool _comparePosition;
        [SerializeField] private Transform _arm1;
        [SerializeField] private Transform _hand1;
        [SerializeField] private Transform _arm2;
        [SerializeField] private Transform _hand2;

        [Header("Break Settings")]
        [Range(1f, 11f)]
        [SerializeField] public float DistanceToBreakHand1 = 3f;
        [SerializeField] public float DistanceToBreakHand2 = 5f;
        [SerializeField] private bool _breakStatus1;
        [SerializeField] private bool _breakStatus2;


        [Header("Reading Info")]
        [SerializeField] private int _movementAmounts = 0;
        [SerializeField] private MTR.Model.Pose _currentPoseToCheck;

        [Header("Current Movement")]
        [SerializeField]
        private MTR.Model.Movement _curMovement;
        [SerializeField] AnimationCurve _movementWeightCurve;

        [Header("Line Debugger")]
        [SerializeField] private LineRenderer _lineHand2ToNextPoint;
        [SerializeField] private LineRenderer _lineHand1ToNextPoint;

        #region PUBLIC METHODS
        public void StartReadingMovement(MTR.Model.Movement movementToRead, Transform initialArm)
        {
            _arm1 = initialArm;
            _hand1 = initialArm.GetChild(0).GetChild(0).transform;

            if(_hand1.childCount > 0)
            {
                _arm2 = initialArm.GetChild(0).GetChild(0).GetChild(0).transform;
                _hand2 = _arm2.GetChild(0).GetChild(0).transform;
            }

            _curMovement = CalculatedMovement(movementToRead);
            _isReading = true;

            StartCoroutine(StartReadingMovementCO(_curMovement));
        }

        public void StopReadingMovement()
        {
            _isReading = false;
        }

        public bool Reading()
        {
            return _isReading;
        }

        public MTR.Model.Movement GetMovementToRead()
        {
            return _curMovement;
        }
        #endregion

        IEnumerator StartReadingMovementCO(MTR.Model.Movement movementToRead)
        {
            while (_isReading)
            {
                if(_showDebugMessage) Debug.Log("Initializing reader with  [" + movementToRead.PoseList.Count + "] poses to Check");

                bool movementIsBroken = false;

                _movementStrengh = 0f;

                OnMovementProgressChanged(movementToRead, _movementStrengh);

                for (int i = 0; i < movementToRead.PoseList.Count; i++)
                {
                    if (_showDebugMessage) Debug.Log("Starting cheking pose [" + i + "] !");

                    _currentPoseToCheck = movementToRead.PoseList[i];
                    _firstPoseOk = false;
                    _secondPoseOk = false;

                    _lineHand1ToNextPoint.SetPosition(1, _currentPoseToCheck.HandPosition);
                    _lineHand2ToNextPoint.SetPosition(1, _currentPoseToCheck.ChildPose.HandPosition);

                    while (!PoseIsCorrect(_currentPoseToCheck))
                    {
                        if (_showDebugMessage) Debug.Log("Waiting for pose [" + i + "] to be correct!");
                        if (_showDebugMessage) Debug.Log("Next check in " + _readerSpeed.ToString("F2") + " seconds");

                        _lineHand1ToNextPoint.SetPosition(0, _hand1.position);
                        _lineHand2ToNextPoint.SetPosition(0, _hand2.position);

                        if(i > 0)
                        {
                            movementIsBroken = MovementHasBroken(_hand1.position, _hand2.position, _currentPoseToCheck);
                            if (movementIsBroken)
                                goto end;
                        }

                        yield return new WaitForSeconds(_readerSpeed);
                    }

                    _movementStrengh = CalculateMovementProgress(i, movementToRead.PoseList.Count);

                    OnMovementProgressChanged(movementToRead, _movementStrengh);
                }

                end:

                if (movementIsBroken)
                    OnMovementBroken(movementToRead, _movementStrengh);
                else
                    OnMovementFinished(movementToRead);

                yield return null;
            }
        }

        private float CalculateMovementProgress(int curPose, int poseAmount)
        {
            if (_showDebugMessage) Debug.LogWarning("Cur Pose [" + curPose +"]" + "Pose Amount [" + poseAmount + "]" + "Progress [" + ((float)curPose/(float)poseAmount).ToString("F2") + "]");
            return ((float)curPose / (float)poseAmount);
        }

        private Model.Movement CalculatedMovement(Model.Movement movementToCalculate)
        {
            Model.Movement movementCalculated = movementToCalculate;

            float poseAmount = movementCalculated.PoseList.Count;

            for (int i = 0; i < poseAmount; i++)
            {
                float weightToUse = _movementWeightCurve.Evaluate((i + 1) / poseAmount);

                movementCalculated.PoseList[i].Weight = weightToUse;
            }

            return movementCalculated;
        }

        #region COMPARE METHODS
        private bool PoseIsCorrect(MTR.Model.Pose poseToCheck)
        {
            bool poseIsCorrect = false;

            if (poseToCheck.ChildPose == null)
            {
                if (_showDebugMessage) Debug.Log("This pose do not have childs");
                _firstPoseOk = ComparePose(poseToCheck, _arm1.rotation, _hand1.position);
                poseIsCorrect = _firstPoseOk;
            }
            else
            {
                if (_showDebugMessage) Debug.Log("This has a child pose");
                _firstPoseOk = ComparePose(poseToCheck, _arm1.rotation, _hand1.position);
                _secondPoseOk = ComparePose(poseToCheck.ChildPose, _arm2.rotation, _hand2.position);
                poseIsCorrect = (_firstPoseOk && _secondPoseOk);
            }

            if (_showDebugMessage) Debug.Log("First Pose Ok: " + _firstPoseOk);
            if (_showDebugMessage) Debug.Log("Second Pose Ok: " + _secondPoseOk);
            return poseIsCorrect;
        }

        private bool ComparePose(MTR.Model.Pose poseToCheck, Quaternion baseRotation, Vector3 basePosition)
        {
            if (_showDebugMessage) Debug.Log("Comparing pose...");
            bool rotationOk = CompareRotation(poseToCheck.ArmRotation, baseRotation);
            bool positionOk = ComparePosition(poseToCheck.HandPosition, basePosition);

            if (_comparePosition && _compareRotation)
                return (rotationOk && positionOk);
            else if (_comparePosition)
            {
                return positionOk;
            }
            else if (_compareRotation)
            {
                return rotationOk;
            }
            else
            {
                return false;
            }
        }

        private bool CompareRotation(Quaternion firstRot, Quaternion secondRot)
        {
            if (!_compareRotation)
                return false;
            else
            {
                if (_showDebugMessage) Debug.Log("Comparing rotation...");
                return firstRot == secondRot;
            }
        }

        private bool ComparePosition(Vector3 firstPos, Vector3 secondPos)
        {
            if (!_comparePosition)
                return false;
            else
            {
                if (_showDebugMessage) Debug.Log("Comparing position...");
                if (_showDebugMessage) Debug.Log("Distance: " + Vector3.Distance(firstPos, secondPos).ToString("F2"));
                return Vector3.Distance(firstPos, secondPos) < DistanceAcceptable;
            }
        }
        #endregion

        #region BREAK METHODS
        public bool MovementHasBroken(Vector3 hand1, Vector3 hand2, MTR.Model.Pose poseToCheck)
        {
            return MovementIsToFar(hand1, hand2, poseToCheck.HandPosition, poseToCheck.ChildPose.HandPosition);
        }

        public bool MovementIsToFar(Vector3 hand1, Vector3 hand2, Vector3 nextPos1, Vector3 nextPos2)
        {
            bool hand1IsFar = _breakStatus1 = Vector3.Distance(hand1, nextPos1) > DistanceToBreakHand1;
            bool hand2IsFar = _breakStatus2 = Vector3.Distance(hand2, nextPos2) > DistanceToBreakHand2;

            return hand1IsFar || hand2IsFar;
        }
        #endregion

        #region LISTENER TRIGGERS
        public void OnPoseComplete(MTR.Model.Movement movement, MTR.Model.Pose poseCompleted, MTR.Model.Pose nextPose)
        {
            throw new NotImplementedException();
        }

        public void OnMovementProgressChanged(MTR.Model.Movement movement, float newProgress)
        {
            MTR.Core.MTRListener.Instance.OnMovementProgressChanged(movement, newProgress);
        }

        public void OnMovementFinished(MTR.Model.Movement movement)
        {
            MTR.Core.MTRListener.Instance.OnMovementFinished(movement);
            _movementAmounts++;
        }

        public void OnMovementBroken(MTR.Model.Movement movement, float progressAchieved)
        {
            MTR.Core.MTRListener.Instance.OnMovementBroken(movement, progressAchieved);
        }
        #endregion
    }
}
