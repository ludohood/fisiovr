﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTR.Core
{
    public class MTRListener : MonoBehaviour
    {
        [SerializeField] private CatapultController _catapult;
        public static MTRListener Instance { get; private set; }

        #region UNITY METHODS
        private void Awake()
        {
            if (Instance)
                Destroy(this.gameObject);
            else
                Instance = this;
        }
        #endregion

        #region ListenerMethods
        public void OnPoseComplete(MTR.Model.Movement movement, MTR.Model.Pose poseCompleted)
        {
            throw new NotImplementedException();
        }

        public void OnMovementProgressChanged(MTR.Model.Movement movement, float newProgress)
        {
            MTR.View.MTRHUD.Instance.ChangeMovementProgress(newProgress);
            _catapult.SetChargeValue(newProgress);
        }

        public void OnMovementFinished(MTR.Model.Movement movement)
        {
            MTR.View.MTRHUD.Instance.FinishMovement(false);
            _catapult.ReleaseProjectile();
        }

        public void OnMovementBroken(MTR.Model.Movement movement, float progressAchieved)
        {
            MTR.View.MTRHUD.Instance.FinishMovement(true);
            _catapult.ReleaseProjectile();
        }
        #endregion
    }
}
