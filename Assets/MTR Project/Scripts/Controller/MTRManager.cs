﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MTR.Model;
using System;

namespace MTR.Core
{
    [RequireComponent(typeof(MTR.View.MTRMovementDebugger))]
    public class MTRManager : MonoBehaviour
    {

        public static MTRManager Instance { get; private set; }

        [Header("Debug Info")]
        [SerializeField] private MTRRecorder _curRecorder;
        [SerializeField] private MTRReader _curReader;

        [Header("Manager Config")]
        [SerializeField] private float _recordFrameRate;
        [SerializeField] private Vector3 _allowedOffSet;
        [SerializeField] private Transform _arm1;
        [SerializeField] private Transform _handObject;

        [Header("MTR Prefabs")]
        [SerializeField] private GameObject _mtrRecorderPrefab;
        [SerializeField] private GameObject _mtrReaderPrefab;

        [Header("Database")]
        [SerializeField] private PoseDatabase _database;

        private void Awake()
        {
            if (Instance)
                Destroy(this.gameObject);
            else
                Instance = this;
        }

        #region RECORDING
        public void StartRecord(string name)
        {
            if (_curRecorder)
                Debug.LogError("Already recording, stop your current first");
            else
            {
                Debug.Log("Starting recording " + _arm1.name + "!");
                _curRecorder = Instantiate(_mtrRecorderPrefab).GetComponent<MTRRecorder>();
                _curRecorder.StartRecording(_arm1, _handObject, _recordFrameRate, name, GenerateGuid());
            }
        }

        public void StopRecord()
        {
            Debug.Log("Stopping recording " + _arm1.name + "!");
            _curRecorder.StopRecording();
            _database.AddMovement(_curRecorder.GetRecordedMovement());

            MTR.View.MTRHUD.Instance.AddNewMovementToList(_curRecorder.GetRecordedMovement());
            MTR.View.MTRMovementDebugger.Instance.NewMovement(_curRecorder.GetRecordedMovement());

            Debug.Log("Movement recorded with " + _curRecorder.GetRecordedMovement().PoseList.Count + " poses!");
            Destroy(_curRecorder.gameObject);
        }

        public bool Recording()
        {
            if (_curRecorder != null)
            {
                return _curRecorder.Recording();
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region READING

        public void StartReading(MTR.Model.Movement movementToRead)
        {
            if (_curReader)
                Debug.LogError("Already reading a movement! [" + _curReader.GetMovementToRead().Name + "]");
            else
            {
                Debug.Log("Starting reading " + movementToRead.Name + "!");
                _curReader = Instantiate(_mtrReaderPrefab).GetComponent<MTRReader>();
                _curReader.StartReadingMovement(movementToRead, _arm1);
            }
        }

        public void StopReading()
        {
            if (!_curReader)
                Debug.LogError("Can stop because Reader is null");
            else
            {
                Debug.Log("Stop reading " + _curReader.GetMovementToRead().Name + "!");
                Destroy(_curReader.gameObject);
            }
        }

        public bool Reading()
        {
            if (_curReader != null)
            {
                return _curReader.Reading();
            }
            else
            {
                return false;
            }
        }

        public MTRReader GetCurrentReader() { return _curReader; }
        #endregion



        private string GenerateGuid()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
