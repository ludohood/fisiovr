﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTR.Core
{
    public class MTRRecorder : MonoBehaviour
    {
        [Header("Debug")]
        [SerializeField] private bool _isRecording;

        [Header("Current Movement")]
        [SerializeField] private MTR.Model.Movement _curMovement;

        #region PUBLIC METHODS
        public void StartRecording(Transform transformToRead, Transform hand, float frameRate, string name, string id)
        {
            _isRecording = true;
            StartCoroutine(StartRecordRotationCO(transformToRead, hand, frameRate, name, id));
        }

        public void StopRecording()
        {
            _isRecording = false;
        }
        public bool Recording()
        {
            return _isRecording;
        }

        public MTR.Model.Movement GetRecordedMovement()
        {
            return _curMovement;
        }
        #endregion

        IEnumerator StartRecordRotationCO(Transform target, Transform hand, float frameRate, string name, string id)
        {
            _curMovement = new MTR.Model.Movement();
            _curMovement.Id = id;
            _curMovement.Name = name;

            while (_isRecording)
            {
                RecordCurRotation(target, hand);
                //Debug.Log("Recorded pose[" + target.rotation + "]");
                Debug.Log("Recorded pose!");
                yield return new WaitForSeconds(frameRate);
            }
        }

        private void RecordCurRotation(Transform target, Transform hand)
        {
            MTR.Model.Pose newPose = new MTR.Model.Pose(hand.position, target.rotation, null);

            if(hand.childCount > 0)
            {
                Transform newChildTransform = hand.GetChild(0).transform;
                MTR.Model.Pose childPose = new MTR.Model.Pose(newChildTransform.GetChild(0).GetChild(0).transform.position, newChildTransform.rotation, null);

                newPose.ChildPose = childPose;
            }

            _curMovement.PoseList.Add(newPose);
        }
    }
}
