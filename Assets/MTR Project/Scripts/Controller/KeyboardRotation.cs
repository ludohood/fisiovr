﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTR.Core
{
    public class KeyboardRotation : MonoBehaviour
    {

        [Header("Object To Rotate")]
        [SerializeField]
        private Transform _objectToRotate;
        [SerializeField] private float _rotateSpeed = 2f;

        [Header("Current Input")]
        [SerializeField]
        private float _XAxis;
        [SerializeField] private float _YAxis;

        // Update is called once per frame
        void Update()
        {
            Rotate();
        }

        private void Rotate()
        {
            _XAxis = Input.GetAxis("Horizontal") * _rotateSpeed * Time.deltaTime;
            _YAxis = Input.GetAxis("Vertical") * _rotateSpeed * Time.deltaTime;

            _objectToRotate.Rotate(new Vector3(_XAxis, _YAxis, 0f));
        }
    }
}
