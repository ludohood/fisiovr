﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTR.Model
{
    [System.Serializable]
    public class Movement
    {
        public string Id;
        public string Name;
        public List<Pose> PoseList = new List<Pose>();
    }
}
