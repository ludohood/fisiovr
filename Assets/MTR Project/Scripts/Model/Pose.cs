﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTR.Model
{
    [System.Serializable]
    public class Pose
    {
        public Vector3 HandPosition;
        public Quaternion ArmRotation;
        public Pose ChildPose;
        public float Weight;

        public Pose(Vector3 position, Quaternion rotation, Pose childPose)
        {
            HandPosition = position;
            ArmRotation = rotation;

            if (childPose != null)
                ChildPose = childPose;
        }
    }
}
