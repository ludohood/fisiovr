﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTR.Core
{
    [System.Serializable]
    public class PoseDatabase
    {
        public List<MTR.Model.Movement> RecordedMovements = new List<MTR.Model.Movement>();

        private void LoadMovements()
        {

        }

        private void SaveMovements()
        {

        }

        public void AddMovement(MTR.Model.Movement movement)
        {
            RecordedMovements.Add(movement);
        }
    }
}
