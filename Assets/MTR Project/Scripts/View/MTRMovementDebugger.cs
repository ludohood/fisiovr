﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MTR.Model;

namespace MTR.View
{
    public class MTRMovementDebugger : MonoBehaviour
    {

        public static MTRMovementDebugger Instance { get; private set; }

        [Header("Debug Target")]
        [SerializeField] private Transform _target;

        [Header("Settings")]
        [SerializeField] private float _sphereSize = 1f;

        [SerializeField] private MTR.Model.Movement _curMovement;

        [SerializeField] private KeyCode _clearKey;

        private void Awake()
        {
            Instance = this;
        }

        private void Update()
        {
            if (Input.GetKeyDown(_clearKey))
                ClearAllPoints();
        }

        public void NewMovement(MTR.Model.Movement movement)
        {
            _curMovement = movement;
        }

        public void ClearAllPoints()
        {
            _curMovement = null;
        }

        private void OnDrawGizmos()
        {
            if (_curMovement != null)
            {
                for (int i = 0; i < _curMovement.PoseList.Count - 2; i++)
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawSphere(_curMovement.PoseList[i + 1].ChildPose.HandPosition, _sphereSize);

                    Gizmos.color = Color.red;
                    Gizmos.DrawLine(_curMovement.PoseList[i].HandPosition, _curMovement.PoseList[i + 1].HandPosition);

                    if(_curMovement.PoseList[i].ChildPose != null)
                    {
                        Gizmos.color = Color.blue;
                        Gizmos.DrawLine(_curMovement.PoseList[i].ChildPose.HandPosition, _curMovement.PoseList[i + 1].ChildPose.HandPosition);
                    }
                }
            }
        }
    }
}
