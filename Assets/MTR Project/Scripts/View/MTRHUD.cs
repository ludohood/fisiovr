﻿using MTR.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTR.View
{
    public class MTRHUD : MonoBehaviour
    {
        public static MTRHUD Instance { get; private set; }

        [Header("Start Reader Panel")]
        [SerializeField] private GameObject _startReaderPanel;
        [SerializeField] private Button _startReaderButton;
        [SerializeField] private Button _stopReaderButton;
        [SerializeField] private Button _closeStartReaderPanelButton;
        [SerializeField] private Text _movementToReadText;

        [Header("Start Recorder Panel")]
        [SerializeField] private GameObject _startRecorderPanel;
        [SerializeField] private Button _startRecorderButton;
        [SerializeField] private Button _stopRecordingButton;
        [SerializeField] private Button _closeStartRecorderPanelButton;
        [SerializeField] private InputField _newMovementInput;

        [Header("Movements Panel")]
        [SerializeField] private Button _newMovementButton;
        [SerializeField] private Transform _movementList;
        [SerializeField] private GameObject _movementItemPrefab;

        [Header("Feedback")]
        [SerializeField] private GameObject _recordFeedback;
        [SerializeField] private GameObject _recordTrail1;
        [SerializeField] private GameObject _recordTrail2;
        [SerializeField] private GameObject _readerFeedback;
        [SerializeField] private GameObject _readerTrail1;
        [SerializeField] private GameObject _readerTrail2;

        [Header("Movement Follower")]
        [SerializeField] private Slider _movementProgressSlider;
        [SerializeField] private Text _movementAmountText;
        [SerializeField] private int _curMovementAmount;

        [Header("Current Reader")]
        [SerializeField] private MTRReader _curReader;
        [SerializeField] private Button _openReaderOptionsButton;
        [SerializeField] private Button _closeReaderOptionsButton;
        [SerializeField] private GameObject _readerPanelUI;
        [SerializeField] private Slider _distanceAcceptableSliderUI;
        [SerializeField] private Text _distanceAcceptableAmountUI;
        [SerializeField] private Slider _distanceToBreakHand1;
        [SerializeField] private Slider _distanceToBreakHand2;

        #region UNITY CALLS
        private void Awake()
        {
            if (Instance)
                Destroy(this.gameObject);
            else
                Instance = this;

            _newMovementButton.onClick.AddListener(() => OpenStartRecorderPanel());

            _closeStartReaderPanelButton.onClick.AddListener(() => CloseStartReaderPanel());
            //start reading is inside open panel method
            _stopReaderButton.onClick.AddListener(() => StopReadingMovement());


            _closeStartRecorderPanelButton.onClick.AddListener(() => CloseStartRecorderPanel());
            _startRecorderButton.onClick.AddListener(() => StarRecordMovement());
            _stopRecordingButton.onClick.AddListener(() => StopRecordingMovement());

            _openReaderOptionsButton.onClick.AddListener(() => OpenReaderOptions());
            _closeReaderOptionsButton.onClick.AddListener(() => CloseReaderOptions());
        }

        // Update is called once per frame
        void Update()
        {
            UpdateFeedback();
        }
        #endregion

        #region RECORDER
        public void StarRecordMovement()
        {
            if (MTR.Core.MTRManager.Instance != null)
            {
                MTR.Core.MTRManager.Instance.StartRecord(_newMovementInput.text);
                _closeStartRecorderPanelButton.onClick.Invoke();
                //_recordTrail1.SetActive(true);
                //_recordTrail2.SetActive(true);
            }
            else
            {
                Debug.LogError("MTR Manager instance is null!");
            }
        }

        public void StopRecordingMovement()
        {
            if (MTR.Core.MTRManager.Instance != null)
            {
                MTR.Core.MTRManager.Instance.StopRecord();
                //_recordTrail1.SetActive(false);
                //_recordTrail2.SetActive(false);
            }
            else
            {
                Debug.LogError("MTR Manager instance is null");
            }
        }

        public void OpenStartRecorderPanel()
        {
            _startRecorderPanel.SetActive(true);
        }

        private void CloseStartRecorderPanel()
        {
            _startRecorderPanel.SetActive(false);
        }
        #endregion

        #region READER
        public void StarReadMovement(MTR.Model.Movement movementToRead)
        {
            if (MTR.Core.MTRManager.Instance != null)
            {
                MTR.Core.MTRManager.Instance.StartReading(movementToRead);
                _closeStartReaderPanelButton.onClick.Invoke();
                _readerTrail1.SetActive(true);
                _readerTrail2.SetActive(true);
                _openReaderOptionsButton.gameObject.SetActive(true);
                _curReader = MTRManager.Instance.GetCurrentReader();

                _distanceAcceptableSliderUI.value = _curReader.DistanceAcceptable;
                _distanceToBreakHand1.value = _curReader.DistanceToBreakHand1;
                _distanceToBreakHand2.value = _curReader.DistanceToBreakHand2;

                _distanceAcceptableSliderUI.onValueChanged.AddListener((float a) => { _curReader.DistanceAcceptable = a; });
                _distanceToBreakHand1.onValueChanged.AddListener((float a) => { _curReader.DistanceToBreakHand1 = a; });
                _distanceToBreakHand2.onValueChanged.AddListener((float a) => { _curReader.DistanceToBreakHand2 = a; });
            }
            else
            {
                Debug.LogError("MTR Manager instance is null");
            }
        }

        public void StopReadingMovement()
        {
            if(MTR.Core.MTRManager.Instance != null)
            {
                MTR.Core.MTRManager.Instance.StopReading();
                _readerTrail1.SetActive(false);
                _readerTrail2.SetActive(false);
                _openReaderOptionsButton.gameObject.SetActive(false);
                _curMovementAmount = 0;
                _movementAmountText.text = _curMovementAmount.ToString();
                _curReader = null;
            }
            else
            {
                Debug.LogError("MTR Manager instance is null");
            }
        }

        public void OpenStartReaderPanel(MTR.Model.Movement movementToRead)
        {
            _movementToReadText.text = movementToRead.Name;

            _startReaderButton.onClick.RemoveAllListeners();
            _startReaderButton.onClick.AddListener(() => StarReadMovement(movementToRead));

            _startReaderPanel.SetActive(true);
        }

        private void CloseStartReaderPanel()
        {
            _startReaderPanel.SetActive(false);
        }

        private void OpenReaderOptions()
        {
            _readerPanelUI.gameObject.SetActive(true);
        }

        private void CloseReaderOptions()
        {
            _readerPanelUI.gameObject.SetActive(false);
        }
        #endregion

        public void AddNewMovementToList(MTR.Model.Movement movementToAdd)
        {
            Button newMovementButton = Instantiate(_movementItemPrefab, _movementList).GetComponent<Button>();
            newMovementButton.onClick.AddListener(() => OpenStartReaderPanel(movementToAdd));
            newMovementButton.GetComponentInChildren<Text>().text = movementToAdd.Name;
        }

        public void ChangeMovementProgress(float newAmount)
        {
            _movementProgressSlider.value = newAmount;
        }

        public void FinishMovement(bool dueBrokenMovement)
        {
            if(!dueBrokenMovement)
                _curMovementAmount++;

            _movementAmountText.text = _curMovementAmount.ToString();
        }

        private void UpdateFeedback()
        {
            if(MTR.Core.MTRManager.Instance != null)
            {
                _recordFeedback.SetActive(MTR.Core.MTRManager.Instance.Recording());
                _readerFeedback.SetActive(MTR.Core.MTRManager.Instance.Reading());
            }
        }
    }
}
