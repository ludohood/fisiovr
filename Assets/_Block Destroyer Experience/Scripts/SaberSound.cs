﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaberSound : MonoBehaviour {

    [Header("Configuration")]
    [SerializeField] private bool _applyVolume;
    [SerializeField] private bool _applyPitch;

    [Header("Debug")]
    [SerializeField] private float _deltaMov;
    [SerializeField] private float _evaluateMov;
    [SerializeField] private float _evaluatePitch;
    [SerializeField] private float _maxDeltaMov;

    [Header("Components")]
    [SerializeField] private Transform _positionReader;
    [SerializeField] private AudioSource _saberAudioSource;
    [SerializeField] private AnimationCurve _deltaCurve;
    [SerializeField] private AnimationCurve _pitchCurve;

    [Header("Calculing Movement")]
    [SerializeField] private Vector3 _curMov;
    [SerializeField] private Vector3 _lastMov;

    // Use this for initialization
    void Start () {

	}

    private void Update()
    {
        ApplyEvaluateOnSound();
    }

    // Update is called once per frame
    void LateUpdate () {

        _curMov = _positionReader.position;

        CalculateDeltaMove();
        CalculateEvaluateMov();
    }

    private void ApplyEvaluateOnSound()
    {
        if(_applyVolume)
            _saberAudioSource.volume = _evaluateMov;

        if (_applyPitch)
            _saberAudioSource.pitch = _evaluatePitch;
    }

    private void CalculateEvaluateMov()
    {
        _evaluateMov = _deltaCurve.Evaluate(_deltaMov);
        _evaluatePitch = _pitchCurve.Evaluate(_deltaMov);
    }

    private void CalculateDeltaMove()
    {
        _deltaMov = Vector3.Distance(_curMov, _lastMov);

        _lastMov = _curMov;

        CalculateMaxDeltaMove();
    }

    private void CalculateMaxDeltaMove()
    {
        if (_deltaMov > _maxDeltaMov)
            _maxDeltaMov = _deltaMov;
    }
}
