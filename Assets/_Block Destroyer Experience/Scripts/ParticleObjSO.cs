﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "Particle Objects Database", menuName="Particle Objects Database")]
public class ParticleObjSO : ScriptableObject
{
	public enum ParticleType
	{
		Explosion,
		Slash
	}
	
	[SerializeField] private List<GameObject> particleObjs;
	[SerializeField] private List<GameObject> explosionParticles;
	[SerializeField] private List<GameObject> slashParticles;

	public GameObject GetParticle(ParticleType particleType)
	{
		GameObject pObj = null;
		switch (particleType)
		{
			case ParticleType.Explosion:
				return explosionParticles[Random.RandomRange(0, explosionParticles.Count)];
				break;
			case ParticleType.Slash:
				return slashParticles[Random.RandomRange(0, slashParticles.Count)];
				break;
			default:
				throw new ArgumentOutOfRangeException("particleType", particleType, null);
		}
		
		return pObj;
	}

}
