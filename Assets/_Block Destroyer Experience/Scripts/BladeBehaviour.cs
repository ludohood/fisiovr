﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeBehaviour : MonoBehaviour
{

    private Rigidbody mRb;
	
    // Use this for initialization
    private Vector3 lastFramePos;
    private Vector3 actualFramePos;

    private Vector3 movementDirection;

    private BladeHit bladeHitScript;

    [SerializeField] private Transform movementRefObj;

    void Start ()
    {
        mRb = GetComponent<Rigidbody>();
        bladeHitScript = GetComponent<BladeHit>();
        
        lastFramePos = Vector3.zero;
        actualFramePos = Vector3.zero;
        movementDirection = Vector3.zero;
        
    }
	
    // Update is called once per frame
    void Update ()
    {
        actualFramePos = movementRefObj.transform.position;
        movementDirection = actualFramePos - lastFramePos;

        
            //movementDirection = Quaternion.AngleAxis(Vector3.Angle(movementDirection, Vector3.forward), Vector3.forward) * Vector3.right ;
        
        lastFramePos = movementRefObj.transform.position;
    }

	

    private void OnCollisionEnter(Collision other)
    {
        CheckHitEnemy(other.gameObject);
		
    }

    private void OnTriggerEnter(Collider other)
    {
        CheckHitEnemy(other.gameObject);
    }

    private void CheckHitEnemy(GameObject objectHitted)
    {
        CubeBehaviour target = objectHitted.GetComponent<CubeBehaviour>();
        if (target != null)
        {
            var vel =  movementDirection;
            bladeHitScript.Hit(objectHitted.transform.position, movementDirection);
            
        }
    }
    
    

}