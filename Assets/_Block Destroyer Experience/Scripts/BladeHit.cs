﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeHit : MonoBehaviour {

	private Vector3 spawnDirection;
	private Vector3 spawnPos;
	
	private GameObject explosionParticle;
	private GameObject slashParticle;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Die()
	{
	}
    
	public void Hit(Vector3 hitPos, Vector3 hitDirection)
	{
		Debug.Log("VELOCITY: " + hitDirection);

		spawnDirection = hitDirection;
		spawnPos = hitPos;
        
		explosionParticle = ParticlesController.Instance.particleDataBase.GetParticle(ParticleObjSO.ParticleType.Explosion);
		slashParticle = ParticlesController.Instance.particleDataBase.GetParticle(ParticleObjSO.ParticleType.Slash);
        
		SpawnDeadParticles();

	}

	private void SpawnDeadParticles()
	{
		ExplosionParticle();
		SlashParticle();
		
	}

	private void ExplosionParticle()
	{
		ParticleSystem ps = InstantiateParticle(explosionParticle,spawnPos, Quaternion.identity);		
	}

	private void SlashParticle()
	{
		ParticleSystem ps = InstantiateParticle(slashParticle, spawnPos, Quaternion.LookRotation(spawnDirection));
		var shapeModule = ps.shape;
		shapeModule.rotation = spawnDirection;
	}

	private ParticleSystem InstantiateParticle(GameObject particleObj, Vector3 position, Quaternion quat)
	{
		GameObject obj = Instantiate(particleObj, position, quat) as GameObject;

		ParticleSystem ps = obj.GetComponent<ParticleSystem>();
		return ps;
	}
}
