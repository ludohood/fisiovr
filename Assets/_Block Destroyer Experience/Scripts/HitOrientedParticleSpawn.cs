﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.XR.WSA.WebCam;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(HitOrientedParticleSpawn))]
public class HitOrientedParticleSpawnEditor : Editor
{
    private HitOrientedParticleSpawn _script;
	
    public override void OnInspectorGUI()
    {
        _script = (HitOrientedParticleSpawn) target;
        DrawDefaultInspector();
		
        if(GUILayout.Button("DIE"))
        {
            _script.Die();
			
        }
		
    }
}

#endif

public class HitOrientedParticleSpawn : MonoBehaviour
{

    [SerializeField] private Vector3 spawnDirection;

    private GameObject explosionParticle;
    private GameObject slashParticle;
	
    // Use this for initialization
    void Start () {
		
    }
	
    // Update is called once per frame
    void Update () {
		
    }

    public void Die()
    {
        Die(spawnDirection);
    }
    
    public void Die(Vector3 hitDirection)
    {
        Debug.Log("VELOCITY: " + hitDirection);

        spawnDirection = hitDirection;
        
        
        explosionParticle = ParticlesController.Instance.particleDataBase.GetParticle(ParticleObjSO.ParticleType.Explosion);
        slashParticle = ParticlesController.Instance.particleDataBase.GetParticle(ParticleObjSO.ParticleType.Slash);
        
        SpawnDeadParticles();

    }

    private void SpawnDeadParticles()
    {
        ExplosionParticle();
        SlashParticle();
		
    }

    private void ExplosionParticle()
    {
        ParticleSystem ps = InstantiateParticle(explosionParticle,transform.position, Quaternion.identity);		
    }

    private void SlashParticle()
    {
        ParticleSystem ps = InstantiateParticle(slashParticle, this.transform.position, Quaternion.LookRotation(spawnDirection));
        var shapeModule = ps.shape;
        shapeModule.rotation = spawnDirection;
    }

    private ParticleSystem InstantiateParticle(GameObject particleObj, Vector3 position,   Quaternion quat, bool asChild = true)
    {
        GameObject obj = Instantiate(particleObj, position, quat, (asChild)?this.transform:null) as GameObject;
		
        ParticleSystem ps = obj.GetComponent<ParticleSystem>();
        return ps;
    }
}