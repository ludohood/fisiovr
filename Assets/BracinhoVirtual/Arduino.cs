﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using Microsoft.Win32;
using UnityEngine.UI;

public enum Mode
{
    Quat,
    Euler
}


public class Arduino : MonoBehaviour
{
    SerialPort stream = new SerialPort();

    [SerializeField] private bool _showDebugMessages;

    int baudrate = 9600;
    int readTimeout = 25;

    public Transform Cube1;
    public Transform Cube2;

    private Quaternion _offeSet1 = Quaternion.identity;
    private Quaternion _offeSet2 = Quaternion.identity;


    public void Start()
    {
        List<string> comports = ComPortNames("2341", "0043");
        if(_showDebugMessages) Debug.Log("Arduinos found: "+comports.Count);
        foreach (var cport in comports)
        {
            if (_showDebugMessages) Debug.Log("Port Name: " + cport);
        }

        OpenArduino(comports.FirstOrDefault());
    }

    void Update()
    {
        string dataString = "null received";

        if (stream.IsOpen)
        {
            try
            {
                dataString = stream.ReadLine();
                if (_showDebugMessages) Debug.Log("RCV_ : " + dataString);
            }
            catch (System.IO.IOException ioe)
            {
                if (_showDebugMessages) Debug.Log("IOException: " + ioe.Message);
            }
        }
        else
        {
            dataString = "NOT OPEN";
//            Debug.Log("RCV_ : " + dataString);
        }


        if (_showDebugMessages) Debug.Log("RCV_ : " + dataString);

        if (!dataString.Equals("NOT OPEN"))
        {
            // recived string is  like  "accx;accy;accz;gyrox;gyroy;gyroz"
            char splitChar = ';';
            string[] dataRaw = dataString.Split(splitChar);

            if (dataRaw[0].Contains("quat"))
            {
                float q1x = float.Parse(dataRaw[1]);
                float q1y = float.Parse(dataRaw[2]);
                float q1z = float.Parse(dataRaw[3]);
                float q1w = float.Parse(dataRaw[4]);

                float q2x = float.Parse(dataRaw[6]);
                float q2y = float.Parse(dataRaw[7]);
                float q2z = float.Parse(dataRaw[8]);
                float q2w = float.Parse(dataRaw[9]);

                Quaternion arduinoQuat1 = new Quaternion(q1y, q1x, q1z, q1w);
                Quaternion arduinoQuat2 = new Quaternion(q2y, q2x, q2z, q2w);

                if (Input.GetKeyDown(KeyCode.R))
                {
                    _offeSet1 = Quaternion.Inverse(arduinoQuat1);
                    _offeSet2 = Quaternion.Inverse(arduinoQuat2);
                }

                Cube1.rotation = Quaternion.Euler(arduinoQuat1.eulerAngles + _offeSet1.eulerAngles);
                Cube2.rotation = Quaternion.Euler(arduinoQuat2.eulerAngles + _offeSet2.eulerAngles);
            }

            else if (dataRaw[0].Contains("euler"))
            {
                float q1x = float.Parse(dataRaw[1]);
                float q1y = float.Parse(dataRaw[2]);
                float q1z = float.Parse(dataRaw[3]);

                float q2x = float.Parse(dataRaw[5]);
                float q2y = float.Parse(dataRaw[6]);
                float q2z = float.Parse(dataRaw[7]);

                Cube1.rotation = (Quaternion.Euler(q1x + 180, q1y + 180, q1z + 180));
                Cube2.rotation = Quaternion.Euler(q2x + 180, q2y + 180, q2z + 180);
            }
        }
    }

    public void OpenArduino(string port)
    {
        // open port. Be shure in unity edit > project settings > player is NET2.0 and not NET2.0Subset
        stream = new SerialPort("\\\\.\\" + port, baudrate);

        try
        {
            stream.ReadTimeout = readTimeout;
            if (_showDebugMessages) Debug.Log("AEHOO");
        }
        catch (System.IO.IOException ioe)
        {
            Debug.Log("IOException: " + ioe.Message);
        }

        stream.Open();
    }

//    public static string AutodetectArduinoPort()
//    {
//        List<string> comports = new List<string>();
//        RegistryKey rk1 = Registry.LocalMachine;
//        RegistryKey rk2 = rk1.OpenSubKey("SYSTEM\\CurrentControlSet\\Enum");
//        string temp;
//        foreach (string s3 in rk2.GetSubKeyNames())
//        {
//            RegistryKey rk3 = rk2.OpenSubKey(s3);
//            foreach (string s in rk3.GetSubKeyNames())
//            {
//                if (s.Contains("VID") && s.Contains("PID"))
//                {
//                    RegistryKey rk4 = rk3.OpenSubKey(s);
//                    foreach (string s2 in rk4.GetSubKeyNames())
//                    {
//                        RegistryKey rk5 = rk4.OpenSubKey(s2);
//                        if ((temp = (string) rk5.GetValue("FriendlyName")) != null && temp.Contains("Arduino"))
//                        {
//                            RegistryKey rk6 = rk5.OpenSubKey("Device Parameters");
//                            if (rk6 != null && (temp = (string) rk6.GetValue("PortName")) != null)
//                            {
//                                comports.Add(temp);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        if (comports.Count > 0)
//        {
//            foreach (string s in SerialPort.GetPortNames())
//            {
//                if (comports.Contains(s))
//                    return s;
//            }
//        }
//
//        return "NotFound";
//    }

    List<string> ComPortNames(String VID, String PID)
    {
        String pattern = String.Format("^VID_{0}.PID_{1}", VID, PID);
        Regex _rx = new Regex(pattern, RegexOptions.IgnoreCase);
        List<string> comports = new List<string>();
        RegistryKey rk1 = Registry.LocalMachine;
        RegistryKey rk2 = rk1.OpenSubKey("SYSTEM\\CurrentControlSet\\Enum");
        foreach (String s3 in rk2.GetSubKeyNames())
        {
            RegistryKey rk3 = rk2.OpenSubKey(s3);
            foreach (String s in rk3.GetSubKeyNames())
            {
                if (_rx.Match(s).Success)
                {
                    RegistryKey rk4 = rk3.OpenSubKey(s);
                    foreach (String s2 in rk4.GetSubKeyNames())
                    {
                        RegistryKey rk5 = rk4.OpenSubKey(s2);
                        RegistryKey rk6 = rk5.OpenSubKey("Device Parameters");
                        comports.Add((string) rk6.GetValue("PortName"));
                    }
                }
            }
        }

        return comports;
    }
}