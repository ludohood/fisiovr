﻿namespace ErugaSFX
{
    using System.Collections.Generic;
    using UnityEngine;
    using System.Linq;
    using System.IO;

    #region EDITOR
    #if UNITY_EDITOR
    using UnityEditor;

    [CustomEditor(typeof(SFXDataScriptable))]
    public sealed class SFXDataScriptableEditor : Editor
    {
        SFXDataScriptable script;
        int count;

        public override void OnInspectorGUI()
        {
            // Header
            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", MonoScript.FromScriptableObject((SFXDataScriptable)target), typeof(SFXDataScriptable), false);
            GUI.enabled = true;
            GUILayout.Space(10);


            // Status
            if (EditorApplication.isCompiling)
            {
                EditorGUILayout.LabelField("Status: Compiling...");
                GUI.enabled = false;
            }
            else if (Application.isPlaying)
                EditorGUILayout.LabelField("Status: Application Running...");
            else
                EditorGUILayout.LabelField("Status: Editable");
            //Update Button
            if (Application.isPlaying) GUI.enabled = false;
            if (GUILayout.Button("Update Codes and Save Data Asset", GUILayout.Height(35)))
            {
                UpdateSFXKey();
            }


            // Getting Target
            script = target as SFXDataScriptable;

            // Debug
            GUI.enabled = true;
            script.Debug = GUILayout.Toggle(script.Debug, "Debug Messages");
            if (Application.isPlaying || EditorApplication.isCompiling) GUI.enabled = false;

            // List of Categories and Elements
            GUILayout.Space(20);
            Categories();
            GUI.enabled = false;

            var skin = GUI.skin.GetStyle("box");
            skin.alignment = TextAnchor.MiddleLeft;

            GUILayout.Space(20);

            GUILayout.Box(
                "Instructions\n\n" +
                "Use the SFX class directly in any script to call some sounds passing \"key\"," +
                "which is an enum generated with categories and keys names:\n\n" +
                "SFXKey.Category_Key\n\n" +
                "d^~^b\n\n" +
                "2D Sound Methods\n" +
                "    PlayOneShot(key, [pitchVariation], [afterPlayAction])\n" +
                "    PlaySpecificOneShot(key, index, [pitchVariation], [afterPlayAction])\n" +
                "    PlayBGM(key, [out BGMkey])\n" +
                "    PlaySpecificBGM(key, index, [out BGMkey])\n" +
                "    StopBGM(BGMkey)\n" +
                "    StopAllBGMs()\n" +
                "\n" +
                "3D Sound Methods\n" +
                "    PlayIn3D(key, origin, [target], [pitchVariation], [afterPlayAction])\n" +
                "    PlaySpecificIn3D(key, index, origin, [target], [pitchVariation], [afterPlayAction]\n\n" +
                " * If you need to change the name or the path of \"[SFXData].asset\" or \"SFXKey.cs\", " +
                "be sure you change the path strings on Consts class in \"SFX.cs\".\n"
                /*, GUILayout.Width(Screen.width * 0.97f)*/);
            //DrawDefaultInspector();
        }

        //Update when unfocus
        private void OnDisable()
        {
            if (!EditorApplication.isCompiling && !Application.isPlaying)
                UpdateSFXKey();
        }

        private void Categories()
        {
            GUILayout.Label("Categories");
            GUILayout.Space(10);

            for (int i = 0; i < script.Categories.Count; i++)
            {
                GUILayout.BeginHorizontal();
                script.Categories[i].Open = EditorGUILayout.Toggle(script.Categories[i].Open, EditorStyles.foldout,GUILayout.MaxWidth(20));
                script.Categories[i].Name = EditorGUILayout.TextField(script.Categories[i].Name, GUILayout.Width(Screen.width * 0.6f));
                if (GUILayout.Button("Remove Category"))
                {
                    script.SFX.RemoveAll(o => o.Category == i);
                    //UpdateSFXKey();
                    foreach (SFXClass sfx in script.SFX)
                    {
                        if (sfx.Category > i) sfx.Category--;
                    }
                    script.Categories.RemoveAt(i);
                    break;
                }
                GUILayout.EndHorizontal();
                if (script.Categories[i].Open)
                {
                    ElementsList(i);
                    GUILayout.Space(30);
                }
            }

            GUILayout.Space(20);

            if (GUILayout.Button("Add New Category"))
            {
                script.Categories.Add(new CategoryClass("NewCategory"));
                //UpdateSFXKey();
            }
        }

        private void ElementsList(int category)
        {
            if (Application.isPlaying) GUI.enabled = false;

            GUILayout.BeginHorizontal();
            GUILayout.Label("Keys", GUILayout.MaxWidth(Screen.width * 0.25f));
            GUILayout.Label("#", GUILayout.Width(20));
            GUILayout.Label("Audio Clips", GUILayout.MaxWidth(Screen.width * 0.45f));

            if (GUILayout.Button("Add Key"))
            {
                SFXClass firstOfnext = script.SFX.Find(o => o.Category == category + 1);
                if (firstOfnext != null)
                {
                    script.SFX.Insert(script.SFX.IndexOf(firstOfnext), new SFXClass(category));
                }
                else
                {
                    script.SFX.Add(new SFXClass(category));
                }
                //UpdateSFXKey();
            }

            GUILayout.EndHorizontal();


            for (int i = 0; i < script.SFX.Count; i++)
            {
                bool first = false;
                bool last = false;
                while (script.SFX[i].Category < category) { i++; first = true; if (i >= script.SFX.Count) { return; } }
                if (i == script.SFX.Count - 1) { last = true; }
                else if (script.SFX[i + 1].Category > category) { last = true; }
                if (script.SFX[i].Category > category) break;

                GUILayout.Space(10);
                GUILayout.BeginHorizontal(GUILayout.ExpandWidth(false));

                if (Application.isPlaying) GUI.enabled = false;
                script.SFX[i].Key = EditorGUILayout.TextField(script.SFX[i].Key, GUILayout.MaxWidth(Screen.width * 0.25f));

                GUI.enabled = !(!(script.SFX[i].Key != "") || EditorApplication.isCompiling);

                GUILayout.BeginVertical(GUILayout.MaxWidth(18));
                if (GUILayout.Button(new GUIContent("+", "Add AudioClip"), GUILayout.MaxHeight(15)))
                {
                    script.SFX[i].Clip.Add(null);
                }
                if (script.SFX[i].Clip.Count > 1)
                {
                    if (GUILayout.Button(new GUIContent("-", "Remove Last AudioClip"), GUILayout.MaxHeight(15)))
                    {
                        script.SFX[i].Clip.RemoveAt(script.SFX[i].Clip.Count - 1);
                    }
                }
                GUILayout.EndVertical();
                
                GUILayout.BeginVertical(GUILayout.MaxWidth(Screen.width * 0.45f));
                for (int j = 0; j < script.SFX[i].Clip.Count; j++)
                {
                    script.SFX[i].Clip[j] = (AudioClip)EditorGUILayout.ObjectField(script.SFX[i].Clip[j], typeof(AudioClip), true);
                }
                GUILayout.EndVertical();
                GUI.enabled = !(EditorApplication.isCompiling || Application.isPlaying);

                if (i == 0 || first) GUI.enabled = false;

                if (GUILayout.Button(new GUIContent("/\\", "Move Up"), GUILayout.MaxWidth(18), GUILayout.MaxHeight(15)))
                {
                    GUI.SetNextControlName("");
                    GUI.FocusControl("");
                    script.MoveItemUp(i);
                    return;
                }
                GUI.enabled = !(EditorApplication.isCompiling || Application.isPlaying);
                if (i == script.SFX.Count - 1 || last) GUI.enabled = false;

                if (GUILayout.Button(new GUIContent("\\/", "Move Down"), GUILayout.MaxWidth(18), GUILayout.MaxHeight(15)))
                {
                    GUI.SetNextControlName("");
                    GUI.FocusControl("");
                    script.MoveItemDown(i);
                    return;
                }

                GUI.enabled = !(EditorApplication.isCompiling || Application.isPlaying);

                Color defaultColor = GUI.color;
                GUI.color = new Color(1f, 0.5f, 0.5f);
                if (GUILayout.Button(new GUIContent("x", "Delete"), GUILayout.MaxWidth(18), GUILayout.MaxHeight(15)))
                {
                    script.SFX.RemoveAt(i);
                    //UpdateSFXKey();
                }
                GUI.color = defaultColor;

                GUILayout.EndHorizontal();
            }
        }

        private void UpdateSFXKey()
        {
            //Categories
            List<string> categories = new List<string>();

            for (int i = 0; i < script.Categories.Count; i++)
            {
                categories.Add(script.Categories[i].Name);
            }

            List<string> auxCategories = new List<string>();
            for (int i = 0; i < categories.Count; i++)
            {
                categories[i] = categories[i].Replace(" ", "");
                if (categories[i] == "" || categories[i].Contains("NewCategory")) categories[i] = "NewCategory";
                auxCategories.Add(categories[i]);
                int count = auxCategories.Where(x => x.Equals(categories[i])).Count();
                if (count > 1)
                {
                    categories[i] = categories[i] + (count);
                }
                script.Categories[i].Name = categories[i];
            }
            //

            //Keys
            List<string> keys = new List<string>();

            for (int i = 0; i < script.SFX.Count; i++)
            {
                keys.Add(script.SFX[i].Key);
            }

            List<string> auxKeys = new List<string>();
            for (int i = 0; i < keys.Count; i++)
            {
                keys[i] = keys[i].Replace(" ", "");
                if (keys[i] == "" || keys[i].Contains("NoKey")) keys[i] = "NoKey";
                keys[i] = categories[script.SFX[i].Category] + "_" + keys[i];
                auxKeys.Add(keys[i]);
                int count = auxKeys.Where(x => x.Equals(keys[i])).Count();
                if (count > 1)
                {
                    keys[i] = keys[i] + (count);
                }
                script.SFX[i].Key = keys[i].Remove(0, categories[script.SFX[i].Category].Length + 1);
            }
            //


            using (StreamWriter outfile = new StreamWriter(Consts.SFXKeyPath))
            {
                string toWrite = "";

                outfile.WriteLine("public enum SFXKey{");

                for (int i = 0; i < keys.Count; i++)
                {
                    toWrite += keys[i] + ((i == keys.Count - 1) ? "\n" : ",\n");
                }

                if (toWrite == "")
                    outfile.WriteLine("NoItems\n}");
                else
                    outfile.WriteLine(toWrite + "\n}");
            }
            GUI.SetNextControlName("");
            GUI.FocusControl("");
            EditorUtility.SetDirty(script);
            AssetDatabase.Refresh();
        }

    }

    #endif
    #endregion

    #region SFXClass
    [System.Serializable]
    public class SFXClass
    {
        public string Key;
        public List<AudioClip> Clip;
        public int Category;
        public SFXClass(int category)
        {
            Category = category;
            Key = "";
            Clip = new List<AudioClip>();
            Clip.Add(null);
        }

    }
    #endregion

    #region CategoryClass

    [System.Serializable]
    public class CategoryClass
    {
        public string Name;
        public bool Open;
        public CategoryClass(string name)
        {
            Name = name;
            Open = false;
        }
    }
    #endregion


    [CreateAssetMenu(menuName = "SFXData")]
    public sealed class SFXDataScriptable : ScriptableObject
    {

        public List<SFXClass> SFX = new List<SFXClass>();
        public List<CategoryClass> Categories = new List<CategoryClass>();

        public bool Debug;

        public void MoveItemUp(int index)
        {
            SFXClass temp = SFX[index];
            SFX.RemoveAt(index);
            SFX.Insert(index - 1, temp);
        }
        public void MoveItemDown(int index)
        {
            SFXClass temp = SFX[index];
            SFX.RemoveAt(index);
            SFX.Insert(index + 1, temp);
        }
    }
}