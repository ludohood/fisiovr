﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ErugaSFX
{

    public sealed class SFXPool : MonoBehaviour
    {

        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }
        public void SetAfterPlay(AudioSource audioSource, Action action)
        {
            StartCoroutine(CallAfterEnd(audioSource, action));
        }

        public IEnumerator CallAfterEnd(AudioSource audioSource, Action action)
        {
            yield return new WaitWhile(() => audioSource.isPlaying);
            action();
        }

        public void SetTargetToFollow(AudioSource audioSource, Vector3 origin, Transform target)
        {
            StartCoroutine(FollowTarget(audioSource, origin, target));
        }

        public IEnumerator FollowTarget(AudioSource audioSource, Vector3 origin, Transform target)
        {
            Transform acTransform = audioSource.transform;
            Vector3 offSet = origin - target.position;
            while (audioSource.isPlaying)
            {
                acTransform.position = target.position + offSet;
                yield return null;
            }
        }

    }
}
