﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using ErugaSFX;

namespace ErugaSFX
{
    public static class Consts
    {
        public const string SFXKeyPath = "Assets/ErugaSFX/DataScripts/SFXKey.cs";
        public const string SFXDataPath = "[SFXData]";
    }
}

public static class SFX
{
    //Properties declaration
    private static Dictionary<string, AudioSource> BGMs = new Dictionary<string, AudioSource>();

    #region private static SFXDataScriptable _data
    private static SFXDataScriptable _data_asset;
    private static SFXDataScriptable _data
    {
        get
        {
            if (_data_asset == null) _data_asset = Resources.Load<SFXDataScriptable>(Consts.SFXDataPath);
            return _data_asset;
        }
        set { }
    }
    #endregion

    #region private static  _poolParent
    private static Transform _poolParent_go;
    private static SFXPool _pool_script;
    /// <summary>
    /// Pool de AudioSources
    /// </summary>
    private static Transform _poolParent
    {
        get
        {
            if (_poolParent_go == null)
            {
                //SceneManager.activeSceneChanged += delegate { NullPool(); };

                _poolParent_go = new GameObject("[SFX AudioSource Pool]", typeof(SFXPool)).transform;
                _pool_script = _poolParent_go.GetComponent<SFXPool>();
                if (_data.Debug) Debug.Log("\"[SFX AudioSource Pool]\" created");
            }
            return _poolParent_go;
        }
        set { }
    }
    #endregion

    //Private Methods
    #region AudioPool related methods

    /// <summary>
    /// Retorna um AudioSource disponível na pool
    /// </summary>
    /// <returns></returns>
    private static AudioSource GetAudioSource()
    {
        List<AudioSource> audioPool = _poolParent.GetComponentsInChildren<AudioSource>().ToList();

        if (audioPool.Count == 0)
            return NewAudioSource();

        foreach (AudioSource audioS in audioPool)
            if (!audioS.isPlaying)
                return audioS;

        return NewAudioSource();
    }

    /// <summary>
    /// Cria um novo AudioSource na pool
    /// </summary>
    /// <returns></returns>
    private static AudioSource NewAudioSource()
    {
        AudioSource newAS = new GameObject("AudioSource", typeof(AudioSource)).GetComponent<AudioSource>();
        newAS.transform.SetParent(_poolParent);
        if (_data.Debug) Debug.Log("Pool updated: " + _poolParent.childCount + " elements");
        return newAS;
    }
    #endregion

    #region Data related methods

    private static AudioClip GetAudioClip(SFXKey key)
    {
        List<AudioClip> audioClip = new List<AudioClip>();
        foreach (AudioClip ac in _data.SFX[(int)key].Clip)
        {
            if (ac) audioClip.Add(ac);
        }

        if (audioClip.Count == 0)
        {
            if (_data.Debug) Debug.Log("No AudioClip");
            return null;
        }

        return audioClip[UnityEngine.Random.Range(0, audioClip.Count)];
    }

    private static AudioClip GetAudioClip(SFXKey key, int index)
    {
        if (index >= _data.SFX[(int)key].Clip.Count) return null;

        return _data.SFX[(int)key].Clip[index];
    }
    #endregion

    // Public Methods
    #region PlayRandomPick

    /// <summary>
    /// Toca um AudioClip da SFXData, com ou sem variação de pitch
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <param name="pitchVariation">Variação do pitch (0 = sem variação)</param>
    /// <param name="afterPlayAction">Action chamada após o audio terminar</param>
    /// <returns></returns>
    public static bool PlayOneShot(SFXKey key, float pitchVariation = 0, Action afterPlayAction = null)
    {
        AudioClip choosedAC;
        if ((choosedAC = GetAudioClip(key)) == null)
        {
            if (_data.Debug) Debug.Log("No AudioClip");
            return false;
        }

        AudioSource audioSource = GetAudioSource();

        audioSource.spatialBlend = 0;

        audioSource.pitch = (pitchVariation == 0) ? 1 : UnityEngine.Random.Range(1 - pitchVariation, 1 + pitchVariation);

        audioSource.PlayOneShot(choosedAC);

        if (afterPlayAction != null) _pool_script.SetAfterPlay(audioSource, afterPlayAction);

        if (_data.Debug) Debug.Log("\"" + choosedAC.name + "\" AudioClip played");
        return true;
    }
    #region PlayOneShot Overloads

    ///// <summary>
    ///// Toca um AudioClip da SFXData, com ou sem variação de pitch
    ///// </summary>
    ///// <param name="key">Key do AudioClip no SFXData</param>
    ///// <param name="pitchVariation">Variação do pitch (0 = sem variação)</param>
    ///// <param name="afterPlayAction">Action chamada após o audio terminar</param>
    ///// <returns></returns>
    //public static bool PlayOneShot(SFXKey key, Action afterPlayAction)
    //{
    //    return PlayOneShot(key, 0, afterPlayAction: afterPlayAction);
    //}
    #endregion


    /// <summary>
    /// Toca um AudioClip da SFXData em loop
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <param name="BGMkey">Referência armazenada deste BGM</param>
    /// <returns></returns>
    public static bool PlayBGM(SFXKey key, out string BGMkey)
    {
        AudioClip choosedAC;
        if ((choosedAC = GetAudioClip(key)) == null)
        {
            if (_data.Debug) Debug.Log("No AudioClip");
            BGMkey = null;
            return false;
        }

        AudioSource audioSource = GetAudioSource();
        BGMkey = choosedAC.name;
        if (BGMs.ContainsKey(BGMkey))
        {
            if (_data.Debug) Debug.Log("\"" + choosedAC.name + "\" BGM already playing");
            return false;
        }
        BGMs.Add(BGMkey, audioSource);

        audioSource.loop = true;
        audioSource.clip = choosedAC;
        audioSource.pitch = 1;
        audioSource.Play();

        if (_data.Debug) Debug.Log("\"" + choosedAC.name + "\" BGM started");
        return true;
    }
    #region PlayBGM Overloads

    /// <summary>
    /// Toca um AudioClip da SFXData em loop
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <returns></returns>
    public static bool PlayBGM(SFXKey key)
    {
        string a;
        return PlayBGM(key, out a);
    }
    #endregion


    /// <summary>
    /// Para de tocar um BGM
    /// </summary>
    /// <param name="BGMkey">Referência do BGM a ser parado</param>
    /// <returns></returns>
    public static bool StopBGM(string BGMkey)
    {
        if (!BGMs.ContainsKey(BGMkey))
        {
            if (_data.Debug) Debug.Log("No BGM found");
            return false;
        }

        BGMs[BGMkey].Stop();
        BGMs[BGMkey].loop = false;
        BGMs.Remove(BGMkey);
        if (_data.Debug) Debug.Log("\"" + BGMkey + "\" BGM stopped");

        return true;
    }


    /// <summary>
    /// Para de tocar todas as BGMs
    /// </summary>
    public static void StopAllBGMs()
    {
        foreach (var element in BGMs)
        {
            element.Value.Stop();
            element.Value.loop = false;
        }
        BGMs.Clear();
        if (_data.Debug) Debug.Log("All BGMs stopped");
    }


    /// <summary>
    /// Toca um AudioClip da SFXData, com ou sem variação de pitch em 3D
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <param name="origin">Ponto de origem do audio</param>
    /// <param name="target">Alvo que o som deve seguir (null se não deve seguir)</param>
    /// <param name="pitchVariation">Variação do pitch (0 = sem variação)</param>
    /// <param name="afterPlayAction">Action chamada após o audio terminar</param>
    /// <returns></returns>
    public static bool PlayIn3D(SFXKey key, Vector3 origin, Transform target = null, float pitchVariation = 0, Action afterPlayAction = null)
    {
        AudioClip choosedAC;
        if ((choosedAC = GetAudioClip(key)) == null)
        {
            if (_data.Debug) Debug.Log("No AudioClip");
            return false;
        }

        AudioSource audioSource = GetAudioSource();

        audioSource.spatialBlend = 1;

        audioSource.pitch = (pitchVariation == 0) ? 1 : UnityEngine.Random.Range(1 - pitchVariation, 1 + pitchVariation);

        audioSource.PlayOneShot(choosedAC);

        if (afterPlayAction != null) _pool_script.SetAfterPlay(audioSource, afterPlayAction);

        if (target) _pool_script.SetTargetToFollow(audioSource, origin, target);
        else audioSource.transform.position = origin;


        if (_data.Debug) Debug.Log("\"" + choosedAC.name + "\" AudioClip played");
        return true;
    }
    #region PlaIn3D overloads

    /// <summary>
    /// Toca um AudioClip da SFXData, com ou sem variação de pitch em 3D
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <param name="origin">Ponto de origem do audio</param>
    /// <param name="target">Alvo que o som deve seguir (null se não deve seguir)</param>
    /// <param name="pitchVariation">Variação do pitch (0 = sem variação)</param>
    /// <param name="afterPlayAction">Action chamada após o audio terminar</param>
    /// <returns></returns>
    public static bool PlayIn3D(SFXKey key, Transform target, float pitchVariation, Action afterPlayAction)
    {
        return PlayIn3D(key, target.position, target, pitchVariation, afterPlayAction);
    }
    ///// <summary>
    ///// Toca um AudioClip da SFXData, com ou sem variação de pitch em 3D
    ///// </summary>
    ///// <param name="key">Key do AudioClip no SFXData</param>
    ///// <param name="origin">Ponto de origem do audio</param>
    ///// <param name="target">Alvo que o som deve seguir (null se não deve seguir)</param>
    ///// <param name="pitchVariation">Variação do pitch (0 = sem variação)</param>
    ///// <param name="afterPlayAction">Action chamada após o audio terminar</param>
    ///// <returns></returns>
    //public static bool PlayIn3D(SFXKey key, Transform target, Action afterPlayAction)
    //{
    //    return PlayIn3D(key, target.position, target, afterPlayAction: afterPlayAction);
    //}
    /// <summary>
    /// Toca um AudioClip da SFXData, com ou sem variação de pitch em 3D
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <param name="origin">Ponto de origem do audio</param>
    /// <param name="target">Alvo que o som deve seguir (null se não deve seguir)</param>
    /// <param name="pitchVariation">Variação do pitch (0 = sem variação)</param>
    /// <param name="afterPlayAction">Action chamada após o audio terminar</param>
    /// <returns></returns>
    public static bool PlayIn3D(SFXKey key, Transform target, float pitchVariation)
    {
        return PlayIn3D(key, target.position, target, pitchVariation);
    }
    #endregion

    #endregion    

    #region PlaySpecificMethods

    /// <summary>
    /// Toca um AudioClip específico da SFXData, com ou sem variação de pitch
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <param name="pitchVariation">Variação do pitch (0 = sem variação)</param>
    /// <param name="afterPlayAction">Action chamada após o audio terminar</param>
    /// <returns></returns>
    public static bool PlaySpecificOneShot(SFXKey key, int index, float pitchVariation = 0, Action afterPlayAction = null)
    {
        AudioClip choosedAC;
        if ((choosedAC = GetAudioClip(key, index)) == null)
        {
            if (_data.Debug) Debug.Log("No AudioClip");
            return false;
        }

        AudioSource audioSource = GetAudioSource();

        audioSource.spatialBlend = 0;

        audioSource.pitch = (pitchVariation == 0) ? 1 : UnityEngine.Random.Range(1 - pitchVariation, 1 + pitchVariation);

        audioSource.PlayOneShot(choosedAC);

        if (afterPlayAction != null) _pool_script.SetAfterPlay(audioSource, afterPlayAction);

        if (_data.Debug) Debug.Log("\"" + choosedAC.name + "\" AudioClip played");
        return true;
    }
    #region PlaySpecificOneShot Overloads
    public static bool PlaySpecificOneShot(SFXKey key, int index, Action afterPlayAction)
    {
        return PlaySpecificOneShot(key, index, afterPlayAction: afterPlayAction);
    }
    #endregion

    /// <summary>
    /// Toca um AudioClip específico da SFXData em loop
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <param name="BGMkey">Referência armazenada deste BGM</param>
    /// <returns></returns>
    public static bool PlaySpecificBGM(SFXKey key, int index, out string BGMkey)
    {
        AudioClip choosedAC;
        if ((choosedAC = GetAudioClip(key, index)) == null)
        {
            if (_data.Debug) Debug.Log("No AudioClip");
            BGMkey = null;
            return false;
        }

        AudioSource audioSource = GetAudioSource();
        BGMkey = choosedAC.name;

        BGMs.Add(BGMkey, audioSource);

        audioSource.loop = true;
        audioSource.clip = choosedAC;
        audioSource.pitch = 1;
        audioSource.Play();

        if (_data.Debug) Debug.Log("\"" + choosedAC.name + "\" BGM started");
        return true;
    }
    #region PlaySpecificBGM Overloads

    public static bool PlaySpecificBGM(SFXKey key, int index)
    {
        string a;
        return PlaySpecificBGM(key, index, out a);
    }
    #endregion

    /// <summary>
    /// Toca um AudioClip específico da SFXData, com ou sem variação de pitch em 3D
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <param name="origin">Ponto de origem do audio</param>
    /// <param name="target">Alvo que o som deve seguir (null se não deve seguir)</param>
    /// <param name="pitchVariation">Variação do pitch (0 = sem variação)</param>
    /// <param name="afterPlayAction">Action chamada após o audio terminar</param>
    /// <returns></returns>
    public static bool PlaySpecificIn3D(SFXKey key, int index, Vector3 origin, Transform target = null, float pitchVariation = 0, Action afterPlayAction = null)
    {
        AudioClip choosedAC;
        if ((choosedAC = GetAudioClip(key, index)) == null)
        {
            if (_data.Debug) Debug.Log("No AudioClip");
            return false;
        }

        AudioSource audioSource = GetAudioSource();

        audioSource.spatialBlend = 1;

        audioSource.pitch = (pitchVariation == 0) ? 1 : UnityEngine.Random.Range(1 - pitchVariation, 1 + pitchVariation);

        audioSource.PlayOneShot(choosedAC);

        if (afterPlayAction != null) _pool_script.SetAfterPlay(audioSource, afterPlayAction);

        if (target) _pool_script.SetTargetToFollow(audioSource, origin, target);
        else audioSource.transform.position = origin;


        if (_data.Debug) Debug.Log("\"" + choosedAC.name + "\" AudioClip played");
        return true;
    }
    #region PlaySpecificIn3D overloads

    /// <summary>
    /// Toca um AudioClip da SFXData, com ou sem variação de pitch em 3D
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <param name="origin">Ponto de origem do audio</param>
    /// <param name="target">Alvo que o som deve seguir (null se não deve seguir)</param>
    /// <param name="pitchVariation">Variação do pitch (0 = sem variação)</param>
    /// <param name="afterPlayAction">Action chamada após o audio terminar</param>
    /// <returns></returns>
    public static bool PlaySpecificIn3D(SFXKey key, int index, Transform target, float pitchVariation, Action afterPlayAction)
    {
        return PlaySpecificIn3D(key, index, target.position, target, pitchVariation, afterPlayAction);
    }
    /// <summary>
    /// Toca um AudioClip da SFXData, com ou sem variação de pitch em 3D
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <param name="origin">Ponto de origem do audio</param>
    /// <param name="target">Alvo que o som deve seguir (null se não deve seguir)</param>
    /// <param name="pitchVariation">Variação do pitch (0 = sem variação)</param>
    /// <param name="afterPlayAction">Action chamada após o audio terminar</param>
    /// <returns></returns>
    public static bool PlaySpecificIn3D(SFXKey key, int index, Transform target, Action afterPlayAction)
    {
        return PlaySpecificIn3D(key, index, target.position, target, afterPlayAction: afterPlayAction);
    }
    /// <summary>
    /// Toca um AudioClip da SFXData, com ou sem variação de pitch em 3D
    /// </summary>
    /// <param name="key">Key do AudioClip no SFXData</param>
    /// <param name="origin">Ponto de origem do audio</param>
    /// <param name="target">Alvo que o som deve seguir (null se não deve seguir)</param>
    /// <param name="pitchVariation">Variação do pitch (0 = sem variação)</param>
    /// <param name="afterPlayAction">Action chamada após o audio terminar</param>
    /// <returns></returns>
    public static bool PlaySpecificIn3D(SFXKey key, int index, Transform target, float pitchVariation)
    {
        return PlaySpecificIn3D(key, index, target.position, target, pitchVariation);
    }
    #endregion

    #endregion

}