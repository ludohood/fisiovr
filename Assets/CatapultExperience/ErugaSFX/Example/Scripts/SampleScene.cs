﻿namespace ErugaSFX
{
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;

    [System.Serializable]
    public class SampleScene : MonoBehaviour
    {
        [SerializeField] private SFXKey sampleSFXKey;

        [SerializeField] private float _circleSpeed = 1;
        [SerializeField] private Transform _circle;
        [SerializeField] private Transform _square;
        [SerializeField] private Transform _squareBGM;
        [SerializeField] private Slider pitchVariation;

        private bool movingToright;

        private string BGMkey = "";

        private string bgmkey;

        private void Update()
        {
            if (Input.GetMouseButtonDown(1))
            {
                PlayIn3D_Sample_2();
            }
            if (_circle.position.x < -5 && !movingToright) movingToright = true;
            if (_circle.position.x > 5 && movingToright) movingToright = false;

            _circle.position += Vector3.right * Time.deltaTime * _circleSpeed * ((movingToright) ? 1 : -1);
        }
        public void PlayOneShot_Sample()
        {
            ChangeColor(_square, Color.blue);
            SFX.PlayOneShot(sampleSFXKey, pitchVariation.value, delegate { ChangeColor(_square, Color.white); });
        }

        public void PlayBGM_Sample()
        {
            ChangeColor(_squareBGM, Color.blue);
            SFX.PlayBGM(sampleSFXKey, out BGMkey);
        }

        public void StopBGM_Sample()
        {
            ChangeColor(_squareBGM, Color.white);
            SFX.StopBGM(BGMkey);
        }
        public void PlayIn3D_Sample_1()
        {
            ChangeColor(_circle, Color.blue);
            SFX.PlayIn3D(sampleSFXKey, _circle, pitchVariation.value, delegate { ChangeColor(_circle, Color.white); });
        }
        public void PlayIn3D_Sample_2()
        {
            SFX.PlayIn3D(sampleSFXKey, Camera.main.ScreenToWorldPoint(Input.mousePosition), pitchVariation: pitchVariation.value);
        }

        public void ChangeColor(Transform form, Color color)
        {
            form.GetComponent<SpriteRenderer>().color = color;
        }

    }
}
