﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils{

	public static float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
	{
		return (float)(x - in_min) * (out_max - out_min) / (float)(in_max - in_min) + out_min;
	}

	[System.Serializable]
	public struct FloatRange
	{
		public float min;
		public float max;
	}
	
	[System.Serializable]
	public struct Vector3Range
	{
		public Vector3 min;
		public Vector3 max;
	}
	
	[System.Serializable]
	public struct IntRange
	{
		public int min;
		public int max;
	}
}
