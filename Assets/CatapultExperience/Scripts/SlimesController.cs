﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class SlimesController : MonoBehaviour
{
    public struct SlimeInfoToRespawn
    {
        public SlimeRace race;
        public Vector3 position;

        public SlimeInfoToRespawn(SlimeRace race, Vector3 position)
        {
            this.race = race;
            this.position = position;
        }
    }

    public SlimeInfoSO SlimeDatabase;

    public static SlimesController Instance = null;

    public Transform EnemiesParent;
    public List<Slime> AllSlimes;

    public Utils.FloatRange TimeSpawnSlime;

    public List<Slime> SlimesCaptured;

    public GameObject capturedOrbFeedback;
    public Transform capturedOrbTarget;

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        AllSlimes = FindObjectsOfType<Slime>().ToList();
        AllSlimes.ForEach(o => o.GetComponentInChildren<Animator>().Play("Born", 0, Random.RandomRange(0f, 1f)));

        if (SlimeDatabase == null)
        {
            Debug.LogError("SLIME DATABASE IS MISSING");
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void GetSlimesNearby(Vector3 HitPoint)
    {
        List<Slime> slimesNearby = AllSlimes.Where(o =>
            Vector3.Distance(o.transform.position, HitPoint) < GameController.Instance.CatchSlimesDistance).ToList();

        List<SlimeInfoToRespawn> newSlimesInfo = null;
        if (slimesNearby.Count > 0)
        {
            newSlimesInfo = new List<SlimeInfoToRespawn>();
            PointsController.Instance.UpdatePoints(slimesNearby.Count);

            slimesNearby.ForEach(o =>
            {
                o.Hit();
                newSlimesInfo.Add(new SlimeInfoToRespawn(o.Race, o.transform.position));
            });

            RespawnSlimes(newSlimesInfo);

            Debug.Log("" + slimesNearby.Count + " SLIMES NEARBY");
        }
    }


    public void SlimeCaptured(Slime slime)
    {
        Debug.LogWarning("SLIME CAPTURED");
        SlimeInfoToRespawn slimeRefill = new SlimeInfoToRespawn(slime.Race, slime.transform.position);

        SpawnDeathParticle(slimeRefill);
        SpawnFeedbackOrb(slimeRefill);

        SlimesCaptured.Add(slime);
        SlimeHit(slime);

        float delay = Random.RandomRange(TimeSpawnSlime.min, TimeSpawnSlime.max);
        
        StartCoroutine(CreateNewSlime(delay,
            SlimeDatabase.GetPossibleSlimesObjs().FirstOrDefault(o => o.GetComponent<Slime>().Race == slimeRefill.race),
            slimeRefill.position));

        
    }

    private void SpawnDeathParticle(SlimeInfoToRespawn slimeInfoRespawn)
    {
        GameObject particle =
            Instantiate(SlimeDatabase.DeathParticle, slimeInfoRespawn.position, Quaternion.identity) as GameObject;
//        particle.GetComponent<ParticleSystem>().startColor = SlimeDatabase.GetSlimeColor(slimeInfoRespawn.race);

        Color color = SlimeDatabase.GetSlimeColor(slimeInfoRespawn.race);
        ParticleSystem.MainModule settings = particle.GetComponent<ParticleSystem>().main;
        settings.startColor = new ParticleSystem.MinMaxGradient(color);
    }

    private void SpawnFeedbackOrb(SlimeInfoToRespawn slimeinfo)
    {
        GameObject orb = Instantiate(capturedOrbFeedback, slimeinfo.position, Quaternion.identity) as GameObject;
        TravelerObject traveler = orb.GetComponent<TravelerObject>();

        UnityEvent eventOnReach = new UnityEvent();
        eventOnReach.AddListener(delegate
        {
            Reached();
            PointsController.Instance.AddPoints(GetPointsByRace(slimeinfo.race));
            
        });
        
        traveler.GoToPosition(capturedOrbTarget.position, SlimeDatabase.GetSlimeColor(slimeinfo.race), eventOnReach);
    }

    private void Reached()
    {
        Debug.LogWarning("ORB REACHED DESTINATION");
    }

    public void SlimeHit(Slime who)
    {
        //GameController.Instance.AddPoints();
        // CreateNewSlime(who.gameObject);
        AllSlimes.Remove(who);
        who.Die();
    }

    IEnumerator CreateNewSlime(float waitTime, GameObject deadGameObject, Vector3 pos)
    {
        yield return new WaitForSeconds(waitTime);
        GameObject slime = Instantiate(deadGameObject, pos, Quaternion.identity) as GameObject;
        slime.transform.parent = EnemiesParent.transform;
    }

    public void SlimeBorn(Slime slime)
    {
        if (AllSlimes.Contains(slime) == false)
        {
            AllSlimes.Add(slime);
        }
    }

    public void RespawnSlimes(List<SlimeInfoToRespawn> slimesToRespawn)
    {
        foreach (var slimeInfoToRespawn in slimesToRespawn)
        {
            float delay = Random.RandomRange(TimeSpawnSlime.min, TimeSpawnSlime.max);
            StartCoroutine(CreateNewSlime(delay,
                SlimeDatabase.GetPossibleSlimesObjs()
                    .FirstOrDefault(o => o.GetComponent<Slime>().Race == slimeInfoToRespawn.race),
                slimeInfoToRespawn.position));
        }
    }


    public float GetPointsByRace(SlimeRace race)
    {
        switch (race)
        {
            case SlimeRace.A:
                return 1f;

            case SlimeRace.B:
                return 2f;
            case SlimeRace.C:
                return 3f;
            default:
                throw new ArgumentOutOfRangeException("race", race, null);
        }
    }
}