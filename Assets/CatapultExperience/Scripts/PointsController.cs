﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsController : MonoBehaviour
{
	
	
	public static PointsController Instance = null;
	
	[SerializeField] private float points = 0f;
	[SerializeField] private AnimationCurve _increasePointsCurve;

	private Coroutine pointsIncreaseCoroutine;
	private float newPoints;
	private bool runningRoutine;

	void Awake()
	{
		Instance = this;
	}
	
	public void UpdatePoints(float pointsGained)
	{
		Debug.Log("ADD " + pointsGained + " Points! New Amount: " + (Points + pointsGained));

		if (runningRoutine)
		{
			points = this.newPoints;
			UpdatePointsAndUI(points);
			StopCoroutine(pointsIncreaseCoroutine);
		}
		pointsIncreaseCoroutine = StartCoroutine(ProgressiveUpdatePoints(Points + pointsGained));
	}


	IEnumerator ProgressiveUpdatePoints(float newPointsAmount)
	{
		float timer = 0f;
		float timeToUpdate = 1f;

		float newPoints = 0f;
		this.newPoints = newPointsAmount;

		runningRoutine = true;
		while (timer < timeToUpdate)
		{
			newPoints = Mathf.Lerp(Points, newPointsAmount, _increasePointsCurve.Evaluate( (timer / timeToUpdate)));
			UpdatePointsAndUI(newPoints);
			timer += Time.deltaTime;
			yield return null;
		}

		points = newPoints;
		runningRoutine = false;
	}

	private void UpdatePointsAndUI()
	{
		UIController.Instance.UpdatePoints(Mathf.Round(points));
	}

	public void AddPoints(float amount)
	{
		points += amount;
		UpdatePointsAndUI(points);
	}

	private void UpdatePointsAndUI(float newPoints)
	{
		UIController.Instance.UpdatePoints(Mathf.Round(newPoints));
	}
	
	public float Points
	{
		get { return points; }
	}
}
