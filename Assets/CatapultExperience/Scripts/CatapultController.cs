﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class CatapultController : MonoBehaviour
{
    public enum CatapultStates
    {
        Idle,
        Charging,
        Throwing,
        Throw
    }

    [Header("Debug")] [SerializeField] private float _chargeForce = 5f;

    #region Components

    private Animator _anim;

    #endregion

    [SerializeField] private CatapultStates _catapultState = CatapultStates.Idle;

    [Range(0, 1)] [SerializeField] private float _chargeValue;

    private float _lastChargedValue;

    [Header("Catapult")] [SerializeField] private GameObject _mesh;
    [SerializeField] private Vector3 _originalPosition;
    [SerializeField] private float _throwImpulseLimitFront = .1f;
    [Range(0f,1f)]
    [SerializeField] private float _timeImpulseFrontThrow = .4f;
    [Range(0f,1f)]
    [SerializeField] private float _timeImpulseBackThrow = .6f;

    private Vector3 _positionAfterThrow;
    
    [Header("Projectile")] [Range(0f, 0.7f)] [SerializeField]
    private float _minimumChargeToThrow;
    [Range(0f, 1f)] [SerializeField] private float _normalizedThrowTime;
    [Range(0f,1f)]
    [SerializeField] private float _refillTime = 0.2f;
    [SerializeField] private ProjectileDataBase _projectileData;
    [SerializeField] private bool _hasProjectileLoaded;
    

    [Header("Audio")] [SerializeField] private AudioSource _audioSource;
    [SerializeField] private List<AudioClip> _chargingAudioClips;

    private Transform _childProjectile;

    
    // Use this for initialization
    void Start()
    {
        _originalPosition = _mesh.transform.position;
        
        LoadComponents();
        _positionAfterThrow = _originalPosition + (_mesh.transform.forward * _throwImpulseLimitFront);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBehaviour();
        if (Input.GetKey(KeyCode.T))
        {
            AddChargeForce(_chargeForce);
        }

        if (Input.GetKeyUp(KeyCode.T))
        {
            StartThrow();
        }
       
    }

    private void UpdateBehaviour()
    {
        switch (_catapultState)
        {
            case CatapultStates.Idle:

                break;
            case CatapultStates.Charging:
                Charging();
                break;
            case CatapultStates.Throwing:
                CheckThrowProjectile();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    #region Catapult Move

    private void MoveFrontThrow()
    {
        
        StartCoroutine(ThrowMoveRoutine(_mesh, _timeImpulseFrontThrow, _originalPosition,_positionAfterThrow, MoveBackThrow));
    }
    private void MoveBackThrow()
    {
        StartCoroutine(ThrowMoveRoutine(_mesh, _timeImpulseBackThrow, _positionAfterThrow,_originalPosition));
    }
    
    IEnumerator ThrowMoveRoutine(GameObject obj, float delayTime, Vector3 originalPos, Vector3 newPos, Action actionAfterFinish = null)
    {
        float timer = 0f;
        Vector3 newTargetPosition = newPos;
        
        while (timer < delayTime)
        {
            obj.transform.position = Vector3.Lerp(originalPos, newTargetPosition, timer / delayTime);    
            timer += Time.deltaTime;
            yield return null;
        }

        if (actionAfterFinish != null)
        {
            actionAfterFinish.Invoke();
        }
            
    }
    
    #endregion
    
    #region Charging

    private void Charging()
    {
        _lastChargedValue = _chargeValue;
        SetChargingValue(_lastChargedValue);
    }

    private void SetChargingValue(float value)
    {
        _anim.SetFloat("ChargeN", _lastChargedValue);
        
        //animInfo.normalizedTime = value;
    }

    public void AddChargeForce(float value)
    {
        float newValue = ChargeValue + value;
        SetChargeValue(newValue);
    }
    
    public void SetChargeValue(float value)
    {
        if (CatapultState == CatapultStates.Idle)
        {
            CatapultState = CatapultStates.Charging;
            
            _audioSource.clip = _chargingAudioClips[Random.Range(0, _chargingAudioClips.Count)];
            _audioSource.Play();
        }

        if (CatapultState == CatapultStates.Charging)
        {
            ChargeValue = value;
            _audioSource.time = _audioSource.clip.length * ChargeValue;
            
            
            
            if (ChargeValue > 1f)
            {
                ChargeValue = 1f;
            }

            if (ChargeValue > _refillTime)
            {
                RefillProjectile();
                
                
            }

            
            
        }
    }
    #endregion

    private void SpawnAndThrowProjectile()
    {
        if (_lastChargedValue > _minimumChargeToThrow)
        {
            _childProjectile.parent = null;
            _childProjectile.GetComponent<ProjectileBehaviour>().Throw(_lastChargedValue, MoveFrontThrow);
            _hasProjectileLoaded = false;
        }
    }
    
    #region Projectile
    
    public void ReleaseProjectile()
    {
        StartThrow();
    }
    
    private void CheckThrowProjectile()
    {
        AnimatorStateInfo animationState = _anim.GetCurrentAnimatorStateInfo(0);
        
        if (animationState.normalizedTime > _normalizedThrowTime)
        {
            SpawnAndThrowProjectile();

//            SFX.PlayOneShot(SFXKey.Catapult_Shot);
            
            _chargeValue = 0f;
            SetChargingValue(_chargeValue);
            _lastChargedValue = 0f;
            SetChargingValue(_lastChargedValue);
            CatapultState = CatapultStates.Idle;
        }
    }

    public void RefillProjectile()
    {
        if(_hasProjectileLoaded == false){
            _hasProjectileLoaded = true;
            GameObject proj = Instantiate(_projectileData.GetDefaultProjectile(), ProjectileController.Instance._projectilePos.position,
                ProjectileController.Instance._projectilePos.rotation);
            proj.GetComponent<Rigidbody>().useGravity = false;
            _childProjectile = proj.transform;
            _childProjectile.SetParent(ProjectileController.Instance._projectilePos);
        }
    }
    #endregion

    #region Throw

    private void StartThrow()
    {             
        ChargeValue = 0f;
        //_anim.SetTrigger("Throw");
        _anim.Play("Throw", 0, 1f - _lastChargedValue);
            
        _audioSource.Stop();
        CatapultState = CatapultStates.Throwing;        
    }

    #endregion

    #region Unity Secondary Calls

    private void LoadComponents()
    {
        _anim = GetComponent<Animator>();
    }

    #endregion

    #region Getters and Setterss

    public CatapultStates CatapultState
    {
        get { return _catapultState; }
        set { _catapultState = value; }
    }

    public float ChargeValue
    {
        get { return _chargeValue; }
        set { _chargeValue = value; }
    }

    #endregion
}