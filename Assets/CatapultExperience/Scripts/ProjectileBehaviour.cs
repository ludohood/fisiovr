﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using Microsoft.Win32;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ProjectileBehaviour : MonoBehaviour
{
    public enum ProjectileStates
    {
        Closed,
        Opened
    }

    private Rigidbody _rb;

    [SerializeField] private float _timeBeforeOpen;
    [SerializeField] private float _timeToOpen;
    [SerializeField] private AnimationCurve _openScaleCurve;

    [SerializeField] private GameObject _closedProjectile;
    [SerializeField] private GameObject _openedProjectile;

    [SerializeField] private Light _halo;
    [SerializeField] private MeshRenderer[] _updateEmissionRenderers;

    [SerializeField] private ProjectileStates _projectileState;

    [SerializeField] private float _emissionChangeStartAt = 0.5f;
    [SerializeField] private float _haloStartAt = 0.5f;
    [SerializeField] private Utils.FloatRange _haloVariation;

    private float catapultCharge;

    // Use this for initialization
    void Start()
    {
        _rb = GetComponent<Rigidbody>();

        _openedProjectile.SetActive(false);

        if (_halo == null)
        {
            _halo = GetComponent<Light>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckDestroy();
        UpdateBehaviour();
    }

    public void Throw(float normalizedForce, Action afterThrowAction = null)
    {
        float force = Utils.mapfloat(normalizedForce, 0f, 1f, ProjectileController.Instance.ThrowForce.min,
            ProjectileController.Instance.ThrowForce.max);

        Debug.Log("Force throw:" + force);
        if (_rb == null)
            _rb = GetComponent<Rigidbody>();

        Vector3 minAngle = ProjectileController.Instance.ThrowAngle.min.normalized;
        Vector3 maxAngle = ProjectileController.Instance.ThrowAngle.max.normalized;


        Vector3 directionByCharge = Vector3.Lerp(minAngle, maxAngle, normalizedForce);
        Vector3 direction = directionByCharge;

        _rb.useGravity = true;
        _rb.AddForce((force * 1000f) * direction);

        if (afterThrowAction != null)
        {
            afterThrowAction.Invoke();
        }

        OpenProjectile();
    }

    private void UpdateBehaviour()
    {
        catapultCharge = FindObjectOfType<CatapultController>().ChargeValue;
        switch (ProjectileState)
        {
            case ProjectileStates.Closed:
                UpdateEmission();
                UpdateHalo();
                break;
            case ProjectileStates.Opened:

                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void UpdateEmission()
    {
        if (catapultCharge > _emissionChangeStartAt)
        {
            float mappedEmission = Utils.mapfloat(catapultCharge, _emissionChangeStartAt, 1f, 0f, 1f);
            Debug.Log("Mapped Emission: " + mappedEmission);
            SetEmissionValue(mappedEmission);
        }
    }

    private void UpdateHalo()
    {
        if (catapultCharge > _haloStartAt)
        {
            float mappedHaloVal = Utils.mapfloat(catapultCharge, _emissionChangeStartAt, 1f, _haloVariation.min, _haloVariation.max);
            SetHaloValue(mappedHaloVal);
        }
    }
    
    private void OpenProjectile()
    {
        StartCoroutine(OpenProjectileAfterDelay());
        ProjectileState = ProjectileStates.Opened;
        _halo.enabled = false;
    }

    IEnumerator OpenProjectileAfterDelay()
    {
        yield return new WaitForSeconds(_timeBeforeOpen);

        _closedProjectile.gameObject.SetActive(false);
        _openedProjectile.gameObject.SetActive(true);

        float timer = 0f;

        while (timer < _timeToOpen)
        {
            _openedProjectile.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one,
                _openScaleCurve.Evaluate((timer / _timeToOpen)));
            timer += Time.deltaTime;
            yield return null;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Slime>())
        {
            SlimesController.Instance.SlimeCaptured(other.GetComponent<Slime>());
            Destroy(this.gameObject);
        }

//        if (other.gameObject.tag == "Floor")
//        {
//            SlimesController.Instance.GetSlimesNearby(transform.position);
//            Destroy(this.gameObject);
//        }
    }


    private void CheckDestroy()
    {
        if (transform.position.y < ProjectileController.Instance.MinimumHeightDestroyProjectiles)
        {
            Destroy(this.gameObject);
        }
    }

    public void SetHaloValue(float value)
    {
        _halo.range = value;
    }

    public void SetEmissionValue(float value)
    {
        Color white = Color.white;
        Color black = Color.black;
        //Color finalColor = Color.Lerp(white, black, value);
        Color finalColor = white * Mathf.LinearToGammaSpace(value);

        foreach (var rend in _updateEmissionRenderers)
        {
            Material mat = rend.material;
            mat.SetColor("_EmissionColor", finalColor);
        }
    }


    public ProjectileStates ProjectileState
    {
        get { return _projectileState; }
        set { _projectileState = value; }
    }
}