﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Projectiles", fileName = "ProjectilesData")]
public class ProjectileDataBase : ScriptableObject
{
    [SerializeField]private GameObject _defaultProjectile;

    [SerializeField]private GameObject[] _projectiles;

    public GameObject GetDefaultProjectile()
    {
        return _defaultProjectile;
    }
    
    public GameObject GetRandomProjectile()
    {
        return _projectiles[Random.RandomRange(0, _projectiles.Length)];
    }
}
