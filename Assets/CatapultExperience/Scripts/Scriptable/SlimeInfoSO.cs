﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[CreateAssetMenu(menuName = "Slime Database", fileName = "Slime Database")]
public class SlimeInfoSO : ScriptableObject {

    [System.Serializable]
    public class SlimeInfo
    {
        public GameObject Obj;
        public Color Color;
    }

    public List<SlimeInfo> Slimes;

    [SerializeField]private GameObject _deathParticle;
    
    public List<GameObject> GetPossibleSlimesObjs()
    {
        List<GameObject> objs = new List<GameObject>();
        objs = Slimes.Select(o => o.Obj).ToList();
        return objs;
    }

    public SlimeInfo GetRandomSlime()
    {
        return Slimes[Random.RandomRange(0, Slimes.Count)];
    }

    public GameObject DeathParticle
    {
        get { return _deathParticle; }
    }

    public Color GetSlimeColor(SlimeRace race)
    {
        Color color = Slimes.Find(o => o.Obj.GetComponent<Slime>().Race == race).Color;
        return color;
    }
}
