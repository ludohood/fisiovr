﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [System.Serializable]
    struct PulseElementRoutine
    {
        public GameObject elementObj;
        [HideInInspector] public Coroutine routine;
        [HideInInspector] public bool updating;
        private Vector3 originalScale;
		
        public PulseElementRoutine(GameObject elementObj) : this()
        {
            this.elementObj = elementObj;
            originalScale = elementObj.transform.localScale;
        }

        public void ResetScale()
        {
            elementObj.transform.localScale = originalScale;
        }
		
        public Vector3 OriginalScale
        {
            get { return originalScale; }
        }
    }


    public static UIController Instance = null;

    [Header("Slime Points")] [SerializeField]
    private Image _slimeCountSprite;
    [SerializeField] private Text _slimeCounterText;
    [SerializeField] private GameObject[] _pointsToPulseObjs;
    [SerializeField]private List<PulseElementRoutine> elementsRoutines;
    [SerializeField] private AnimationCurve pulseUICurve;
    [SerializeField] private float timeToPulse = 0.2f;
	
    private void Awake()
    {
        Instance = this;
    }
	
    // Use this for initialization
    void Start () {
        UpdatePoints();
        Init();	
    }

    private void Init()
    {		
        foreach (var el in _pointsToPulseObjs)
        {
            elementsRoutines.Add(new PulseElementRoutine(el));
        }
    }
	
    public void UpdatePoints()
    {
        string newPoints = PointsController.Instance.Points.ToString();
		
        if(newPoints != _slimeCounterText.text )
            PulseUiPoints();
        _slimeCounterText.text = PointsController.Instance.Points.ToString();
    }

    public void UpdatePoints(float pointsAmount)
    {
        string newPoints = pointsAmount.ToString();
		
        if(newPoints != _slimeCounterText.text )
            PulseUiPoints();
        
        _slimeCounterText.text = pointsAmount.ToString();		
    }

    private void PulseUiPoints()
    {
        foreach (var elementsRoutine in elementsRoutines)
        {
            if (elementsRoutine.updating)
            {
                elementsRoutine.ResetScale();
                StopCoroutine(elementsRoutine.routine);
            }

            StartCoroutine(PulseUiElement(elementsRoutine));
        }
    }

    private IEnumerator PulseUiElement(PulseElementRoutine element)
    {
        float timer = 0f;
        while (timer < timeToPulse)
        {
            element.elementObj.transform.localScale =
                element.OriginalScale * pulseUICurve.Evaluate(timer / timeToPulse);
			
            timer += Time.deltaTime;
            yield return null;
        }

        element.elementObj.transform.localEulerAngles = element.OriginalScale;
    }
	
	
	
}