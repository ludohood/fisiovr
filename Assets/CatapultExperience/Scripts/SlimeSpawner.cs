﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;

public class SlimeSpawner : MonoBehaviour
{

	public GameObject objToSpawn;
	public Utils.IntRange _amount;

	public void Spawn()
	{
		int amount = Random.RandomRange(_amount.min, _amount.max);
		

		for (var i = 0; i < amount; i++)
		{
//			Debug.Log("SPawn");
			Vector3 pos;
			float sizeX = gameObject.transform.lossyScale.x;
			float sizeZ = gameObject.transform.lossyScale.z;

			float randomX = transform.position.x + Random.Range(-sizeX / 2f, sizeX / 2f);
			float randomZ = transform.position.z + Random.Range(-sizeZ / 2f, sizeZ / 2f);
			Vector3 newPos = new Vector3(randomX, 0f, randomZ);

			pos = newPos;
			
			GameObject obj = Instantiate(objToSpawn, pos, Quaternion.identity) as GameObject;
			obj.transform.SetParent(this.transform);
			
			SetToFloor(obj);
		}
		
		
	}

	void SetToFloor(GameObject obj)
	{
		float newPosY = 0f;
		
		RaycastHit hit;
		Debug.DrawRay(obj.transform.position, -Vector3.up * 5, Color.green);

		if (Physics.Raycast(transform.position, -Vector3.up, out hit))
		{
			newPosY = hit.point.y;
//			Debug.Log("HIT");
	
		}

		obj.transform.position = new Vector3(obj.transform.position.x, newPosY, obj.transform.position.z);
	}

	// Use this for initialization
	void Start () {
		Spawn();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
