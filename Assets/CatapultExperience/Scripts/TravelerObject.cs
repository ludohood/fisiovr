﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.Events;

public class TravelerObject : MonoBehaviour
{
    public Vector3 targetPos;
    public AnimationCurve travelCurve;

    // Use this for initialization
    void Start()
    {
        if (travelCurve.length <= 1)
        {
            travelCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void GoToPosition(Vector3 target, Color slimeColor, UnityEvent onReach = null)
    {
        targetPos = target;
        GetComponent<MeshRenderer>().material.color = slimeColor;
        StartCoroutine(GoToPositionRoutine(2f, onReach));
    }

    IEnumerator GoToPositionRoutine(float timeDelay, UnityEvent onReach = null)
    {
        Vector3 actualPos = transform.position;

        float timer = 0f;

        while (timer < timeDelay)
        {
            Vector3 newPos = Vector3.Lerp(actualPos, targetPos, travelCurve.Evaluate((timer / timeDelay)));
            transform.position = newPos;
            timer += Time.deltaTime;
            yield return null;
        }

        if (onReach != null)
        {
            onReach.Invoke();
        }
    }
}