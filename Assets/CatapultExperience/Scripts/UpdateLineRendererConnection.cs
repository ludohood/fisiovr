﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLineRendererConnection : MonoBehaviour
{

	public Transform OriginPoint;
	public Transform TargetPoint;

	private LineRenderer _lineRenderer;
	
	// Use this for initialization
	void Start ()
	{
		_lineRenderer = GetComponent<LineRenderer>();		
		_lineRenderer.SetPositions(new Vector3[]{OriginPoint.position, TargetPoint.position});
	}

	// Update is called once per frame
	void Update () {
		_lineRenderer.SetPositions(new Vector3[]{OriginPoint.position, TargetPoint.position});
	}
}
