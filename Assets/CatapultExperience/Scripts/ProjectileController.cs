﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{

	public static ProjectileController Instance = null;

	public Utils.FloatRange ThrowForce;
	public Utils.Vector3Range ThrowAngle;

	public Transform _projectilePos;

	public float MinimumHeightDestroyProjectiles = -1f;
	
	void Awake()
	{
		Instance = this;
	}

	private void OnDrawGizmos()
	{
		Gizmos.DrawRay(_projectilePos.position, ThrowAngle.min.normalized);
		Gizmos.DrawRay(_projectilePos.position, ThrowAngle.max.normalized);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
}
