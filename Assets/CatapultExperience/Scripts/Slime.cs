﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SlimeRace
{
	A,
	B,
	C
}

public class Slime : MonoBehaviour
{			
	private SlimesController sC;

	[SerializeField] private SlimeRace _race;
	// Use this for initialization
	public void Start ()
	{
		
		SlimesController.Instance.SlimeBorn(this);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnEnable()
	{
		
	}

	public void Hit()
	{
		
		Debug.Log("Controller; " + SlimesController.Instance + " - this: " + this);
		SlimesController.Instance.SlimeHit(this);
	}

	public void Die()
	{
		Destroy(this.gameObject);
		
	}

	public SlimeRace Race
	{
		get { return _race; }
	}
}
